"""

    Author: Martin Horak
    Python Version: 3.5
    OpenCV Version: 3.1.0
    Description: This file contains function and variables that are specific for the QFN package component

"""

from Ultron.component import component
from Ultron.defect.defect_void import Void
from Ultron.utilities import output

import cv2
import numpy as np
import sys
from collections import namedtuple
from Ultron.enums import EdgeDetectioMethod
from Ultron.utilities import image_operations
from Ultron.component.component import Component
import os
import configparser

qfn_package_collection = namedtuple('Int', ['x', 'y', 'width', 'height', 'angle', 'voids'])


class QFNPackage(Component):
    def __init__(self, array_componet_rectangles):
        super().__init__(array_componet_rectangles)
        self.voids = [Void() for i in range(len(array_componet_rectangles))]

    def __getitem__(self, item):
        return qfn_package_collection(x=self.x[item], y=self.y[item], width=self.width[item], height=self.height[item],
                                      angle=self.angle[item], voids=self.voids[item])

    # def analyze_voids(self, image, method,
    #                   clahe_clip_limit=2,
    #                   clahe_tile_grid_size=(5, 5),
    #                   closing_ksize=5,
    #                   closing_iterations=2,
    #                   filter_small_contours=True,
    #                   debug_mode=True,
    #                   fxy=1):

    def analyze_voids(self,
                      image,
                      method,
                      configuration,
                      debug_mode=True,
                      fxy=1):
        """
         :param configuration: configuration file that contains all following variables
                 clahe_clip_limit: CLAHE parameter
                 clahe_tile_grid_size: CLAHE parameter
                 closing_ksize: kernel size of the morphological function
                 closing_iterations: number of iterations of the morphological function
                 filter_small_contours: boolean
         :param method: method that will be used for the void analysis
         :param fxy: resize ratio
         :param image: input 2D xray image
         :param debug_mode: if True: it will print additional information and images
         :return: None
        """

        # check whether the config file exists
        try:
            if not os.path.isfile(configuration):
                raise IOError('The entered path of the configuration file does not exists.')
        except IOError:
            print('Entered path: ', configuration)
            raise

        config = configparser.ConfigParser()
        config.read(configuration)
        try:
            if config.has_section('analyze_voids'):
                clahe_clip_limit = int(config['analyze_voids']['clahe_clip_limit'])
                clahe_tile_grid_size = tuple([int(i) for i in config['analyze_voids']['clahe_tile_grid_size'].split(',')])
                closing_ksize = int(config['analyze_voids']['closing_ksize'])
                closing_iterations = int(config['analyze_voids']['closing_iterations'])
                filter_small_contours = bool(config['analyze_voids']['filter_small_contours'])
            else:
                raise ValueError('The configuration file must contain section: analyze_voids')
        except KeyError:
            print('One of the following variables is missing in the configuration file: '
                  'clahe_clip_limit, clahe_tile_grid_size, closing_ksize, closing_iterations, filter_small_contours')
            raise
        except ValueError:
            raise


        for package in self:
            qfn_component = ((package.x, package.y), (package.width, package.height), package.angle)
            # debug print
            output.debug_show_rot_rects("Component", image, [qfn_component], debug_mode=False, waitkey=True,
                                        fxy=fxy)

            cropped = self.crop_qfn_package(image, qfn_component, debug_mode=False)
            package.voids.contours, package.voids.hierarchy = self.find_voids_inside_component(
                                                            cropped,
                                                            method=method,
                                                            clahe_clip_limit=clahe_clip_limit,
                                                            clahe_tile_grid_size=clahe_tile_grid_size,
                                                            closing_ksize=closing_ksize,
                                                            closing_iterations=closing_iterations,
                                                            filter_small_contours=filter_small_contours,
                                                            debug_mode=debug_mode,
                                                            fxy=fxy)


            # ToDo: It is possible to create one universal function for both solderball and component
            package.voids.ratio = self.__calculate_void_ratio(package.voids.contours, package.voids.hierarchy,
                                                              d=cropped)

    def crop_qfn_package(self, image, qfn_component, debug_mode=False, fxy=1, k_size=5, iterations=5):
        """
        It will crop the component from the image + adds a white background

        :type iterations:
        :param iterations:
        :param k_size:
        :param image:
        :param qfn_component:
        :param debug_mode:
        :param fxy:
        :return:
        """

        (x, y), (width, height), angle = qfn_component
        croppe_component = image[int(y - height / 2):int(y + height / 2), int(x - width / 2):int(x + width / 2)]

        # debug print
        output.debug_show("cropped qfn package", croppe_component, debug_mode=debug_mode, waitkey=False)

        # convert to gray scale
        gray = cv2.cvtColor(croppe_component, cv2.COLOR_BGR2GRAY)

        # use OTSU thresholding
        binary_img = image_operations.use_thresholding(gray, method=3)

        output.debug_show("Binary image", binary_img, debug_mode=debug_mode, waitkey=False)

        # use morphological closing that will filter out small objects including BGA
        kernel = np.ones((k_size, k_size), np.uint8)
        closing = cv2.morphologyEx(binary_img, cv2.MORPH_CLOSE, kernel, iterations=iterations)

        output.debug_show("After closing", closing, debug_mode=debug_mode, waitkey=False)

        # find contours
        im2, contours, hierarchy = cv2.findContours(closing.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

        output.show_contours("Contours", croppe_component.copy(), contours, debug_mode=debug_mode, fxy=fxy,
                             waitkey=False)

        # pick just the contour of the component (the contour that is second highest in the hierarchy)

        # ToDo: Check how is the hierarchy matrix made. At this ponint I suggest that the the contour that I am looking
        #  for is second in the hierarchy
        qfn_contour = contours[1]
        # for index, cnt in enumerate(contours):
        #     if hierarchy[0][index,3] == -1:
        #         qfn_contour.append(cnt)
        #     else:
        #         continue

        output.show_contours("QFN Contours", croppe_component.copy(), qfn_contour, debug_mode=debug_mode, fxy=fxy,
                             waitkey=False)

        qfn_component_mask = np.zeros((int(height), int(width)), np.uint8)

        # replace with the fill contour
        cv2.drawContours(qfn_component_mask, [qfn_contour], 0, (255, 255, 255), thickness=-1)

        # debug print
        output.debug_show("Mask", qfn_component_mask.copy(), debug_mode=debug_mode, fxy=fxy, waitkey=False)

        foreground = cv2.bitwise_and(croppe_component, croppe_component, mask=qfn_component_mask)
        foreground = cv2.cvtColor(foreground, cv2.COLOR_BGR2GRAY)
        # crop the background
        background_mask = cv2.bitwise_not(qfn_component_mask)
        cropped = cv2.bitwise_or(background_mask, foreground)

        # debug print
        output.debug_show("After cropping", cropped.copy(), debug_mode=debug_mode, fxy=fxy, waitkey=True)

        return cropped

    def __calculate_void_ratio(self, contours, hierarchy, d):
        component_area = 0
        void_area = 0
        ball_contour_count = 0
        for index, contour in enumerate(contours):
            # d_color = cv2.cvtColor(d, cv2.COLOR_GRAY2BGR)
            # output.show_contours("EVERY FUCKING CONTUOUR", d_color, contour)
            if hierarchy[index][3] == -1:
                # its a ball contour
                component_area += cv2.contourArea(contour)
                ball_contour_count += 1
            else:
                # it is a void contour
                void_area += cv2.contourArea(contour)

        # print(void_area, "    ", ball_area)
        # calculate the ratio
        if component_area > 0:
            ratio = (void_area / component_area) * 100
        else:
            # in case of problem we assign -1 which doesnt make sence considering the ratio value
            ratio = -1
        if ball_contour_count != 1:
            # in case of problem we assign -1 which doesnt make sence considering the ratio value
            return -1

        else:
            return ratio

    def show_all_voids(self, image, error_ratio, fxy=1, window_name="Voids", waitkey=True):
        image_copy = image.copy()
        for package in self:
            qfn_component = ((package.x, package.y), (package.width, package.height), package.angle)
            output.draw_voids(image_copy, qfn_component, package.voids.ratio, package.voids.contours,
                              error_ratio, contour_thickness=3, font_scale=5)

        resized = cv2.resize(image_copy, (0, 0), fx=fxy, fy=fxy, interpolation=cv2.INTER_LINEAR)
        cv2.imshow(window_name, resized)
        if waitkey:
            cv2.waitKey()
