"""

    Author: Martin Horak
    Python Version: 3.5
    OpenCV Version: 3.1.0
    Description: This file contains function and variables that are specific for the Solder Ball package component

"""


import collections
import math
import os
import configparser

import cv2
import numpy as np

from Ultron.component.component import Component
from Ultron.defect.defect_void import Void
from Ultron.utilities import output
from Ultron.enums import EdgeDetectioMethod

solderball_collection = collections.namedtuple('Int', ['x', 'y', 'width', 'height', 'angle', 'radius', 'void'])


class SolderBalls(Component):
    """
    This class asociates all properties of solder ball (x,y, radius) and functions for solder ball analysis

    """

    def __init__(self, solderball_array):
        boundary_boxes = []

        # constructor gets array of solderballs [[x,y,radius], ...,...]
        for solderball in solderball_array:
            x = solderball[0]
            y = solderball[1]
            width = solderball[2] * 2
            height = solderball[2] * 2
            angle = 0

            boundary_box = ((x, y), (width, height), angle)
            boundary_boxes.append(boundary_box)

        # x = solderball_array[:, 0]
        # y = solderball_array[:, 1]
        # radius = solderball_array[:, 2]

        super().__init__(boundary_boxes)
        self.radius = [ball[2] for ball in solderball_array]
        self.void = [Void() for i in range(len(solderball_array))]


        # # angle parameter of circle will be always 0 because rotation of circle is nonsence
        # self.angle = np.zeros(solderball_array.shape[0])

    def __getitem__(self, item):
        return solderball_collection(x=self.x[item], y=self.y[item], width=self.width[item], height=self.height[item],
                                     angle=self.angle[item], radius=self.radius[item], void=self.void[item])

    def __len__(self):
        """
        This finction will calculate number of items (solderballs) in the object
        :return: inteeger representing number of items
        """
        if len(self.angle) == len(self.height) == len(self.radius) == len(self.void) == len(self.width) == \
                len(self.x) == len(self.y):
            return len(self.x)
        else:
            return -1

    # def analyze_voids(self, image, method,
    #                   clahe_clip_limit=1,
    #                   clahe_tile_grid_size=(5, 5),
    #                   closing_ksize=(2, 2),
    #                   closing_iterations=2,
    #                   fxy=3,
    #                   debug_mode=False):

    def analyze_voids(self, image, method,
                      configuration,
                      fxy=3,
                      debug_mode=False):
        """
         :param configuration: configuration file that contains all following variables
             closing_iterations: number of itteration of morphological closing
             clahe_clip_limit: CLAHE parameter
             clahe_tile_grid_size: CLAHE parameter
             closing_ksize: size of the kernel of morphological closing
         :param method: method that will be used for void analysis
         :param fxy: resize ratio
         :param image: input 2D xray image
         :param debug_mode: if True: it will print additional information and images
         :return: None
        """

        # check whether the config file exists
        try:
            if not os.path.isfile(configuration):
                raise IOError('The entered path of the configuration file does not exists.')
        except IOError:
            print('Entered path: ', configuration)
            raise

        config = configparser.ConfigParser()
        config.read(configuration)
        try:
            if config.has_section('analyze_voids'):

                clahe_clip_limit = int(config['analyze_voids']['clahe_clip_limit'])
                clahe_tile_grid_size = tuple([int(i) for i in config['analyze_voids']['clahe_tile_grid_size'].split(',')])
                closing_ksize = tuple([int(i) for i in config['analyze_voids']['closing_ksize'].split(',')])
                closing_iterations = int(config['analyze_voids']['closing_iterations'])
            else:
                raise ValueError('The configuration file must contain section: analyze_voids')
        except KeyError:
            print('One of the following variables is missing in the configuration file: '
                  'clahe_clip_limit, clahe_tile_grid_size, closing_ksize, closing_iterations')
            raise
        except ValueError:
            raise


        for i in range(len(self)):
            circle = [self.x[i], self.y[i], self.radius[i]]
            cropped = self.crop_solderball(image, circle, debug_mode=debug_mode)
            self.void[i].contours, self.void[i].hierarchy = self.find_voids_inside_component(
                                                            cropped,
                                                            method=method,
                                                            clahe_clip_limit=clahe_clip_limit,
                                                            clahe_tile_grid_size=clahe_tile_grid_size,
                                                            closing_ksize=closing_ksize,
                                                            closing_iterations=closing_iterations,
                                                            debug_mode=debug_mode,
                                                            fxy=fxy)

            # some hierarchy is 3D and some not. This is the  way how to solve it
            if len(self.void[i].hierarchy.shape) == 2:
                self.void[i].hierarchy = np.expand_dims(self.void[i].hierarchy, axis=0)

            self.void[i].ratio = self.__calculate_void_ratio(self.void[i].contours, self.void[i].hierarchy)



            # Todo: Use shape criterium to filter out non-circular shapes
            # Todo: Find out how to regulate sensitivity

    def append(self):
        pass

    def average_radius(self):
        return int(np.median(self.radius))

    def __calculate_void_ratio(self, contours, hierarchy):
        ball_area = 0
        void_area = 0
        ball_contour_count = 0



        if hierarchy.shape[1] < 2:
            return -1



        for index, contour in enumerate(contours):
            if hierarchy[0, index, 2] == -1:
                # it is a void contour
                void_area += cv2.contourArea(contour)
            else:
                # its a ball contour
                ball_area += cv2.contourArea(contour)
                ball_contour_count += 1
        # print(void_area, "    ", ball_area)
        # calculate the ratio
        if ball_area > 0:
            # ratio = (void_area/ball_area)*100
            # PARAM: self.average_radius might be a problem if the PCB contains solder balls of different size
            average_ball_area = pow(self.average_radius(), 2) * math.pi
            ratio = (void_area / average_ball_area) * 100
        else:
            # in case of problem we assign -1 which doesnt make sence considering the ratio value
            ratio = -1
        if ball_contour_count != 1:
            # in case of problem we assign -1 which doesnt make sence considering the ratio value
            return -1

        else:
            return ratio

    @staticmethod
    def crop_solderball(image, circle, debug_mode=False):
        """
        It will gets a coordinates of a circle (x,y,radius) and the image and crop the solder ball and adds a white
        background
        :param circle:
        :param debug_mode:
        :param image:
        :return:
        """
        # image parameters
        x_coordinate = circle[0]
        y_coordinate = circle[1]
        radius = circle[2]
        height, width = image.shape[:2]
        # crop the solder ball
        circle_mask = np.zeros((height, width), np.uint8)
        cv2.circle(circle_mask, (int(x_coordinate), int(y_coordinate)), int(radius), 255, thickness=-1)
        foreground = cv2.bitwise_and(image, image, mask=circle_mask)
        foreground = cv2.cvtColor(foreground, cv2.COLOR_BGR2GRAY)

        # crop the background
        background_mask = cv2.bitwise_not(circle_mask)
        cropped = cv2.bitwise_or(background_mask, foreground)
        radius += 2
        cropped = cropped[y_coordinate - radius:y_coordinate + radius, x_coordinate - radius:x_coordinate + radius]
        if debug_mode:
            # Debug print
            temp_0 = cv2.resize(cropped, None, fx=3, fy=3, interpolation=cv2.INTER_CUBIC)
            cv2.imshow("Original", temp_0)
            # cv2.waitKey()
        return cropped

    def save_data2file(self):
        pass

    def show_one_void(self):
        pass

    def show_all_voids(self, image, error_ratio, window_name="Voids", waitkey=True):
        image_copy = image.copy()
        for item in self:
            circle = [item.x, item.y, item.radius]
            output.draw_circular_voids(image_copy, circle, item.void.ratio, item.void.contours, item.void.hierarchy,
                                       error_thresh=error_ratio)

        cv2.imshow(window_name, image_copy)
        if waitkey:
            cv2.waitKey()
