"""

    Author: Martin Horak
    Python Version: 3.5
    OpenCV Version: 3.1.0
    Description: Create superclass "Component" that will contain variables and functions to describe basic properties of
                 the components on the PCB board

"""

import collections
import numpy as np
import cv2
import numpy
import sys

from Ultron.utilities import output
from Ultron.enums import EdgeDetectioMethod

component_collection = collections.namedtuple('Int', ['x', 'y', 'width', 'height', 'angle'])


class Component(object):
    """
    This class associates all common properties of objects

    """

    def __init__(self, boundary_rectangle):
        # x, y, width, height, angle
        # [rect[1][0] for rect in rot_rects_big]
        self.x = [rect[0][0] for rect in boundary_rectangle]
        self.y = [rect[0][1] for rect in boundary_rectangle]
        self.width = [rect[1][0] for rect in boundary_rectangle]
        self.height = [rect[1][1] for rect in boundary_rectangle]
        self.angle = [rect[2] for rect in boundary_rectangle]

        # QUESTION is this necessary? everythong works also without the super() but according to the pychram all
        # subclasses should contain super method
        # super().__init__(self.original_image)

    def __getitem__(self, item):
        return component_collection(x=self.x[item], y=self.y[item], width=self.width[item], height=self.height[item],
                                    angle=self.angle[item])

    def __len__(self):
        """
        This finction will calculate number of items (solderballs) in the object
        :return: inteeger representing number of items
        """
        if len(self.angle) == len(self.height) == len(self.width) == len(self.x) == len(self.y):
            return len(self.x)
        else:
            return 0

    def analyze_shift(self, image):
        """

        :param image:
        :return:
        """
        pass

    def is_missing(self):
        """
        This method will compare tested image with the reference image and checks if the component is not
        missing

        :return:
        """
        pass

    def find_voids_inside_component(self,
                                    cropped,
                                    method,
                                    clahe_clip_limit=2,
                                    clahe_tile_grid_size=(3, 3),
                                    closing_ksize=3,
                                    closing_iterations=1,
                                    debug_mode=False,
                                    filter_small_contours=False,
                                    fxy=3):
        """
        This fuction calculates the ratio between the void area and the ball area
        :param filter_small_contours:
        :param blur_kernel:
        :param method: The method that can be used to segment the voids. User can choole between the LoG, DoG or
                        Canny edge detector.
        :param fxy: resize factor
        :param closing_iterations: number of itterations of morphological function
        :param closing_ksize: size of kernel of morphological function
        :param blur_ksize: size of kernel size for LoG
        :param clahe_tile_grid_size:
        :param clahe_clip_limit:
        :param cropped: input image
        :param debug_mode: if True it will print additional info
        :return: contours, hierarchy
        """
        output.debug_show("Original image", cropped, debug_mode=debug_mode, fxy=fxy, waitkey=False)

        # PARAM: Median blur before enhancing image
        # get rid of salt-and-pepper noise using the median filter
        median_blur = cv2.medianBlur(cropped, ksize=5)

        # debug print
        output.debug_show("Median blur", median_blur, debug_mode=debug_mode, fxy=fxy, waitkey=False)

        blur = cv2.GaussianBlur(cropped, (3, 3), 0)

        # debug print
        output.debug_show("Gauss blur", blur, debug_mode=debug_mode, fxy=fxy, waitkey=False)

        # improve the local contrast using CLAHE
        # create a CLAHE object (Arguments are optional).
        # PARAM: Contrast Limited Adaptive Histogram Equalization
        #  clipLimit – Threshold for contrast limiting.
        #  tileGridSize – Size of grid for histogram equalization. Input image will be divided into equally sized
        #                       rectangular tiles. tileGridSize defines the number of tiles in row and column.

        # good values (clipLimit=2.0, tileGridSize=(3, 3))
        clahe = cv2.createCLAHE(clipLimit=clahe_clip_limit, tileGridSize=clahe_tile_grid_size)
        # it is allso possible to use gaussian blur
        cl1 = clahe.apply(median_blur)

        # debug print
        output.debug_show("Enhanced Image", cl1, debug_mode=debug_mode, fxy=fxy, waitkey=False)

        # debug print -> convert gray scale to colormap
        color_map = cv2.applyColorMap(cl1, cv2.COLORMAP_JET)
        output.debug_show("Color map", color_map, debug_mode=debug_mode, fxy=fxy, waitkey=False)

        # find edges on the image
        edges = method.detect_edges(cl1)
        output.debug_show("After edge detection", edges, debug_mode=debug_mode, fxy=fxy, waitkey=False)


        # use closing
        kernel = np.ones((closing_ksize), np.uint8)
        closing = cv2.morphologyEx(edges, cv2.MORPH_CLOSE, kernel, iterations=closing_iterations)

        # debug print
        output.debug_show("Closing", closing.copy(), debug_mode=debug_mode, fxy=fxy, waitkey=False)

        # get contours
        im2, contours, hierarchy = cv2.findContours(closing, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

        void_contours = []
        for index, cnt in enumerate(contours):
            if hierarchy[0][index, 3] != -1:
                void_contours.append(cnt)
        # debug printr
        closing_color = cv2.cvtColor(closing, cv2.COLOR_GRAY2BGR)
        output.show_contours("Contours of the void", closing_color, void_contours, debug_mode=debug_mode, fxy=fxy,
                             waitkey=False, thickness=1)

        # debug print
        median_blur_color = cv2.cvtColor(median_blur, cv2.COLOR_GRAY2BGR)
        output.show_contours("Contours - result", median_blur_color, contours, debug_mode=debug_mode, fxy=fxy,
                             waitkey=True, thickness=1)

        if filter_small_contours:
            # filter out contours that which are is above the average
            contours_filtered, hierarchy_filtered = self.filter_small_conours(contours, hierarchy)

            # debug print
            cropped_color = cv2.cvtColor(cropped, cv2.COLOR_GRAY2BGR)
            output.show_contours("After filtering small contours", cropped_color, contours_filtered,
                                 debug_mode=debug_mode, fxy=fxy, waitkey=True, thickness=1)
        else:
            contours_filtered = contours
            hierarchy_filtered = hierarchy

        # in case of using regular thresholding istead of edge detector it is necessary to get rid of the outer contour
        # delete this conotur and set its first child's parrrnt to -1
        if type(method).__name__ == 'MethodThreshold':
            for index, cnt in enumerate(contours_filtered):
                if hierarchy_filtered[0][index, 3] == -1:
                    # find child and set its parrent to -1
                    child = hierarchy_filtered[0][index, 2]
                    hierarchy_filtered[0][child, 3] = -1

                    #delete this contour
                    hierarchy_filtered = np.delete(hierarchy_filtered, index, 1)
                    contours_filtered = np.delete(contours_filtered, index, 0)

                    hierarchy_filtered = hierarchy_filtered[0]

                    break

        # ToDo: Approximate contours to me them more circular
        # # aproximate conoturs
        # contours_approximated = []
        # for cnt in contours_filtered:
        #     epsilon = 0.001 * cv2.arcLength(cnt, True)
        #     approx = cv2.approxPolyDP(cnt, epsilon, True)
        #     contours_approximated.append(approx)
        #
        # # debug print
        # cropped_color = cv2.cvtColor(cropped, cv2.COLOR_GRAY2BGR)
        # output.show_contours("After approximation", cropped_color, contours_approximated,
        #                      debug_mode=debug_mode, fxy=fxy, waitkey=True, thickness=1)

        # filter out contours that dont have circular shape
        # circularity_coeficient_threshold = 7
        # contours_filtered_circularity = self.filter_noncircular_contours(circularity_coeficient_threshold,
        #                                                                  contours_filtered)
        # # debug print
        # cropped_color = cv2.cvtColor(cropped, cv2.COLOR_GRAY2BGR)
        # output.show_contours("After filtering non circular contours", cropped_color, contours_filtered_circularity, debug_mode=debug_mode,
        #                      fxy=fxy, waitkey=True, thickness=1)

        return contours_filtered, hierarchy_filtered

    def filter_noncircular_contours(self, circularity_coeficient_threshold, contours_filtered):
        """

        :param circularity_coeficient_threshold:  threshold value for circularity
        :param contours_filtered: input contours
        :return: filtered contours
        """
        circularity_coeficients_all = []
        contours_filtered_circularity = []
        for cnt in contours_filtered:

            perimeter = cv2.arcLength(cnt, True)
            area = cv2.contourArea(cnt)
            circularity = (perimeter * perimeter) / (4 * np.pi * area)
            circularity_coeficients_all.append(circularity)
            if circularity < circularity_coeficient_threshold:
                contours_filtered_circularity.append(cnt)

        return contours_filtered_circularity

    def filter_small_conours(self, contours, hierarchy):
        """
        Nasty function. Be careful using that :D

        :param contours:
        :param hierarchy:
        :return:
        """

        contours_good = []
        for index, cnt in enumerate(contours):
            if hierarchy[0][index, 3] == -1:
                continue
            else:
                contours_good.append(cnt)

        contour_area = [cv2.contourArea(cnt) for cnt in contours_good]
        average_area = np.mean(contour_area)

        contours_filtered = []
        hierarchy_filtered = []
        for index, cnt in enumerate(contours):
            if cv2.contourArea(cnt) > average_area:
                contours_filtered.append(cnt)
                hierarchy_filtered.append(hierarchy[0, index, :])

        return contours_filtered, hierarchy_filtered
