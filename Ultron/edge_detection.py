import cv2
from Ultron.utilities import output


class EdgeDetectionMethod(object):
    pass

    def detect_edges(self, image):
        pass


class MethodDoG(EdgeDetectionMethod):

    def __init__(self, blur_kernel1, blur_kernel2):
        self.blur_kernel1 = blur_kernel1
        self.blur_kernel2 = blur_kernel2

    def detect_edges(self, image, debug_mode=False, fxy=1, waitkey=True):
        # PARAM: DoG Parameter is typically applied when the size ratio of kernel (2) to kernel (1) is 4:1 or 5:1
        # blur1 = cv2.GaussianBlur(cl1, (5, 5), 0)
        # blur2 = cv2.GaussianBlur(cl1, (19, 19), 0)
        blur1 = cv2.GaussianBlur(image, (self.blur_kernel1, self.blur_kernel1), 0)
        blur2 = cv2.GaussianBlur(image, (self.blur_kernel2, self.blur_kernel2), 0)
        edges = blur2 - blur1


        # debug print
        output.debug_show("DoG edge detector result", edges, debug_mode=debug_mode, fxy=fxy, waitkey=waitkey)

        return edges


class MethodLoG(EdgeDetectionMethod):

    def __init__(self, gaus_blur_kernel, laplace_kernel):
        self.gaus_blur_kernel = gaus_blur_kernel
        self.laplace_kernel = laplace_kernel

    def detect_edges(self, image, debug_mode=False, fxy=1, waitkey=True):
        blur = cv2.GaussianBlur(image, (self.gaus_blur_kernel, self.gaus_blur_kernel), 0)
        edges = cv2.Laplacian(blur, cv2.CV_8U, ksize=self.laplace_kernel)

        output.debug_show("LoG edge detector result", edges, debug_mode=debug_mode, fxy=fxy, waitkey=waitkey)

        return edges


class MethodCanny(EdgeDetectionMethod):

    def __init__(self, threshold1=25, threshold2=50, apertureSize=3, L2gradient=True):
        self.threshold1 = threshold1
        self.threshold2 = threshold2
        self.apertureSize = apertureSize
        self.L2gradient = L2gradient

    def detect_edges(self, image, debug_mode=False, fxy=1, waitkey=True):
        # ToDo: Try more edge detectors Solber, Canny
        edges = cv2.Canny(image, threshold1=self.threshold1, threshold2=self.threshold2,
                          apertureSize=self.apertureSize, L2gradient=self.L2gradient)

        output.debug_show("Canny edge detector result", edges, debug_mode=debug_mode, fxy=fxy, waitkey=waitkey)

class MethodThreshold(EdgeDetectionMethod):

    def __init__(self, threshold=100):
        self.threshold = threshold


    def detect_edges(self, image, debug_mode=False, fxy=1, waitkey=True):
        ret1, binary_image = cv2.threshold(image, self.threshold, 255, cv2.THRESH_BINARY)


        # output.debug_show("Thresholding result", binary_image, debug_mode=True, fxy=3, waitkey=True)
        return binary_image
