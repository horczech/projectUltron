"""

    Author: Martin Horak
    Python Version: 3.5
    OpenCV Version: 3.1.0
    Description: The functions and variables for the super class of the  "defect" object

"""

class Defect(object):

    def __init__(self):
        pass

    def is_fatal(self):
        pass
