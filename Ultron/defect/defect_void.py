"""

    Author: Martin Horak
    Python Version: 3.5
    OpenCV Version: 3.1.0
    Description: The functions and variables for the "Void defect" object

"""

class Void(object):
    def __init__(self, void_ratio=None, contours=None, hierarchy=None):
        self.ratio = void_ratio
        self.contours = contours
        self.hierarchy = hierarchy

    def is_fatal(self):
        pass
