"""

    Author: Martin Horak
    Python Version: 3.5
    OpenCV Version: 3.1.0
    Description: The functions and variables for the "missing component" object

"""

from Ultron.defect.defect import Defect


class MissingComponentDefect(Defect):
    pass