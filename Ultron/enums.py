"""

    Author: Martin Horak
    Python Version: 3.5
    OpenCV Version: 3.1.0
    Description: This file contains the enums that are used in the program

"""

from enum import Enum


class EdgeDetectioMethod(Enum):
    DoG = "DoG"
    LoG = "LoG"
    Canny = "Canny"


class AlignMethod(Enum):
    SIFT = "SIFT"
    ECC = "ECC"


class CircleDetectionMethod(Enum):
    HoughtCircles = "1"
    BoundaryRectangles = "2"
