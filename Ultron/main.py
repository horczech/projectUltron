"""

	Author: Martin Horak
    Python Version: 3.5
    OpenCV Version: 3.1.0
    Description: The purpose of this file is to demonstrate how to use Ultron library.

"""

import cv2
from Ultron.utilities import output
from Ultron.enums import EdgeDetectioMethod
from Ultron.utilities import find_functions
from Ultron.utilities import image_operations
from Ultron.enums import CircleDetectionMethod
import os
from pathlib import Path
from Ultron import edge_detection
from Ultron.enums import AlignMethod

SCALE = 0.2
root_folder_path = str(Path(__file__).parents[1])

# ----------- SOLDER BALL TEST -----------------
#
# image_path = os.path.join(root_folder_path, 'tests', 'test_images', 'fig1.jpg')
# image = cv2.imread(image_path)
# output.debug_show("Original", image, fxy=SCALE)
# config_file_path = os.path.join(root_folder_path, 'configuration_files', 'config_solderballs.ini')
#
# solderballs = find_functions.find_solderballs(image, method=CircleDetectionMethod.BoundaryRectangles,
#                                               configuration=config_file_path,
#                                               debug_mode=False,
#                                               fxy=SCALE)
#
# method = edge_detection.MethodLoG(gaus_blur_kernel=15, laplace_kernel=13)
# # method = edge_detection.MethodThreshold(threshold=155)
# solderballs.analyze_voids(image, method=method,
#                           configuration=config_file_path,
#                           fxy=3,
#                           debug_mode=True)
#
# solderballs.show_all_voids(image, error_ratio=17, window_name='Result')


# ------------ QNF TEST ---------------

# image_path = os.path.join(root_folder_path, 'tests', 'test_images', 'fig8.png')
# original_image = cv2.imread(image_path)
# config_file_path = os.path.join(root_folder_path, 'configuration_files', 'config_qfn_package.ini')
#
# output.debug_show("Original", original_image, fxy=SCALE)
#
# qnf_packages = find_functions.find_qnf_package(original_image,
#                                                configuration=config_file_path,
#                                                debug_mode=False,
#                                                fxy=SCALE)
#
# method = edge_detection.MethodLoG(gaus_blur_kernel=51, laplace_kernel=11)
# # method = edge_detection.MethodThreshold(threshold=45)
# qnf_packages.analyze_voids(original_image,
#                            configuration=config_file_path,
#                            method=method,
#                            debug_mode=True,
#                            fxy=1)
# qnf_packages.show_all_voids(original_image, error_ratio=25, fxy=SCALE)


# ------------ IMAGE SUBTRACTION TEST ---------------


base_image_path = os.path.join(root_folder_path, 'images', 'test_subtraction', 'PCB1', '[2]120kV-2000uA-4s.png')
test_image_path = os.path.join(root_folder_path, 'images', 'test_subtraction', 'PCB1', '[11]120kV-2000uA-4s.png')
config_file_path = os.path.join(root_folder_path, 'configuration_files', 'config_image_subtraction.ini')

base_image = cv2.imread(base_image_path)
test_image = cv2.imread(test_image_path)

output.debug_show("Input base image", base_image, fxy=SCALE)
output.debug_show("Input test image", test_image, fxy=SCALE)


base_img, test_img = image_operations.crop_and_align_images(base_image, test_image,
                                                            align_method=AlignMethod.ECC,
                                                            configuration=config_file_path,
                                                            debug_mode=False,
                                                            fxy=SCALE)

output.debug_show("Base Image Aligned", base_img, fxy=SCALE)
output.debug_show("Test Image Aligned", test_img, fxy=SCALE)


# output.show_missing_components(base_img, test_img, debug_mode=False, fxy=SCALE)
# output.show_redundant_components(base_img, test_img, debug_mode=False, fxy=SCALE)
output.show_all_defects(base_img, test_img, debug_mode=True, fxy=SCALE)
