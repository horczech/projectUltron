"""

    Author: Martin Horak
    Python Version: 3.5
    OpenCV Version: 3.1.0
    Description: This file contains supportive functions

"""

import sys
import cv2
import numpy as np
import math
from Ultron.utilities import output
import time

def timeit(f):
    """
    timer of functions
    :param f: function that will be measured
    :return: time of the process
    """

    def timed(*args, **kw):

        ts = time.time()
        result = f(*args, **kw)
        te = time.time()
        print("...........................")
        print('\nTIMER: Func:%r took: %2.4f sec\n' % (f.__name__, te-ts))
        print("...........................")

        return result

    return timed


@timeit
def get_hough_circles(binary_img, fxy=1, method=1):
    """
    This function finds the circles on the binary image

    :param method: number that defines which method will be used to find the circles
            1: Using HoughCircles -> there are 3 parameters that can affect the result
                                    (dp=1, minDist=20, param1=125, param2=30)
            2: Not implemented

    :param fxy: resize factor (to speed up hought circles)
    :param binary_img: input binary image that

    :return n x 3 array with x,y,r (x centre coordinate, y centre coordinate, radius)
            - all values have to be converted to int64 because they reperesent pixels on the image

    """
    if method == 1:
        # find the circles using the HoughCircles

        # PARAM: Hough Circles
        # method – Detection method to use. Currently, the only implemented method is CV_HOUGH_GRADIENT ,
        # which is basically 21HT , described in [Yuen90].
        #
        # dp – Inverse ratio of the accumulator resolution to the image resolution. For example, if dp=1 , the
        # accumulator has the same resolution as the input image. If dp=2 , the accumulator has half as
        # big width and height.
        #
        # minDist – Minimum distance between the centers of the detected circles. If the parameter is too small,
        # multiple neighbor circles may be falsely detected in addition to a true one. If it is too large, some
        # circles may be missed.
        #
        # param1 – First method-specific parameter. In case of CV_HOUGH_GRADIENT , it is the higher threshold of
        # the two passed to the Canny() edge detector (the lower one is twice smaller).
        #
        # param2 – Second method-specific parameter. In case of CV_HOUGH_GRADIENT , it is the accumulator
        # threshold for the circle centers at the detection stage. The smaller it is, the more false circles may
        # be detected. Circles, corresponding to the larger accumulator values, will be returned first.
        #
        # minRadius – Minimum circle radius.
        #
        # maxRadius – Maximum circle radius.

        small = cv2.resize(binary_img, (0, 0), fx=fxy, fy=fxy, interpolation=cv2.INTER_LINEAR)
        factor = 1 / fxy

        temp = cv2.GaussianBlur(small, (3, 3), 0)

        circles = cv2.HoughCircles(temp, cv2.HOUGH_GRADIENT, dp=1, minDist=10, param1=125, param2=30)
        if (circles is None) or not (circles.any()):
            sys.exit("No circles found !")
        else:
            circles *= factor

    else:
        sys.exit("Wrong input method. Check the get_circles fuction.")

    return np.int64(circles[0, :, :])

@timeit
def get_contour_circles(binary_img, circle_hist=0.1):
    """
     This function finds the circles on the binary image

    :param binary_img: input binary image that

    :param circle_hist: ratio between the width and hight of bounding rectangle that is drawn around the
                        contour. If the contour is ideal rectangle the bounding rectangle will be square and
                        ration between width and hight will be 1. circle_hist is the amount of nonsymetry of the
                        found circle.

    :return n x 3 array with x,y,r (x centre coordinate, y centre coordinate, radius)
            - all values have to be converted to int64 because they reperesent pixels on the image
    """

    # Extract Contours
    im2, contours, hierarchy = cv2.findContours(binary_img.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)

    rectangles = np.empty((0, 5), dtype=int)
    circles = np.empty((0, 3), dtype=int)
    for cnt in contours:
        x, y, w, h = cv2.boundingRect(cnt)
        ratio = w / h

        # PARAM: Circle histeresis of the bounding rectangle (see description of method)This parameter shouldnt
        # affect the result
        if (ratio > (1 - circle_hist)) and (ratio < (1 + circle_hist)):
            rectangles = np.append(rectangles, [[x, y, w, h, w * h]], axis=0)
            circles = np.append(circles, [[int(x + w / 2), int(y + h / 2), int((h + w) / 4)]], axis=0)

    return circles


def is_solderball(binary_image, circle, hist=0.2, ratio=25):
    """
        :param ratio: used in the intensity test. is is ratio between white and black pixels. For more info check the
                        filter_intensity fuction
        :param hist: used in the integrity check to filter out too small or too big circles. For more info check the
                    filter_small_circles fuction
        :param binary_image: we will use it to check the ratio between white and bleck pixels in the filter_intensity
                            fuction
        :param circle: the circle 1x3 array of potential solder balls it contains [x, y, radius]
        :return: boolean True - if the circle meets all requirements
        """

    # filter out circles that dont have 100% of its area in the image
    integrity_test = filter_incomplete_circles(circle, binary_image)

    # Check the intensity distribution inside
    # each sircle (portion between white and black circles)
    # We are looking for circles where majority of pixels is black = 0
    intensity_test = filter_intensity(circle, binary_image, ratio=25)

    # filter out too small or too big circles
    # if the circle is smaller than 1-hist or bigger than 1+hist that it is removed from array
    # self.average_radius = np.median(self.solderballs[:, 2])
    # size_test = self.filter_small_circles(circle, hist)
    size_test = True

    if size_test and integrity_test and intensity_test:
        return True
    else:
        return False


def filter_incomplete_circles(circle, binary_image):
    """
        This function will check if the the whole circle is on the image

        :param circle: tested circle array 1x3 [x,y,radius]
        :return: boolean True -> tested circle is OK

        """

    height, width = binary_image.shape
    # test top edge
    up = (circle[0] - circle[2]) >= 0
    # test left edge
    left = (circle[1] - circle[2]) >= 0
    # test right edge
    right = (circle[0] + circle[2]) <= width
    # test bottom edge
    down = (circle[1] + circle[2]) <= height

    if up and down and left and right:
        return True
    else:
        return False

        # # test top edge
        # up = (self.solderballs[:, 0] - self.solderballs[:, 2]) >= 0
        # # test left edge
        # left = (self.solderballs[:, 1] - self.solderballs[:, 2]) >= 0
        # # test right edge
        # right = (self.solderballs[:, 0] + self.solderballs[:, 2]) <= width
        # # test bottom edge
        # down = (self.solderballs[:, 1] + self.solderballs[:, 2]) <= height
        #
        # self.solderballs = self.solderballs[(up & down & right & left), :]


def filter_intensity(circle, binary_img, ratio=25):
    """
    The function checks the ratio of while and black pixels (white/black) in the circle. Circles that have majority
    white are considered as wrong

    :param circle: x,y coordinates and radius of circle
    :param ratio: ration of white color in the ball (circle), solder ball should be black
    :param binary_img: binary image
    :return: boolean True -  solder ball; Flase - Something else
    """

    x_coordinate = circle[0]
    y_coordinate = circle[1]
    radius = circle[2]
    height, width = binary_img.shape

    circle_img = np.zeros((height, width), np.uint8)
    cv2.circle(circle_img, (x_coordinate, y_coordinate), radius, 1, thickness=-1)

    masked_data = cv2.bitwise_and(binary_img, binary_img, mask=circle_img)
    # crop_masked_data = img[y_coordinate-radius:y_coordinate+radius, x_coordinate-radius:x_coordinate+radius]

    circle_area = len(circle_img[circle_img == 1])
    white_area = len(masked_data[masked_data > 0])

    if circle_area == 0:
        """if the area of circle is zero then the whole circle is out of image frame"""
        return False

    elif ((white_area / circle_area) * 100) < ratio:
        """if the percentage of white area in the found circle is under 25% it is a solderball"""
        return True

    else:
        return False


def delaunay_triangulation(points, size):
    """

    :param points: (x,y) coordinates
    :return: subdiv2D object with the triangulation result
    """

    # Rectangle to be used with Subdiv2D
    rect = (0, 0, size[1], size[0])

    # Create an instance of Subdiv2D
    subdiv = cv2.Subdiv2D(rect)

    # Insert points into subdiv
    for p in points:
        subdiv.insert(tuple(p))

    return subdiv


def get_missing_circles2(subdiv, binary_img, solderballs, average_radius, size, ratio=25):
    """

    :param subdiv: input parameter calculated using the triangulation
    :param binary_img: will be used to check the intensity of inside the circle
    :param ratio: ration between white and black pixels
    :return: array of missing circle nx3 [x, y, radius]
    """
    edge_list = subdiv.getEdgeList()
    r = (0, 0, size[1], size[0])
    found_circles = np.empty((0, 3), dtype=int)

    binary_img_copy = binary_img.copy()
    binary_img_copy = cv2.cvtColor(binary_img_copy, cv2.COLOR_GRAY2BGR)

    # browse every edge of tricngle
    for i, value in enumerate(edge_list):
        # pt1 - centre
        # pt2 - pont that will be rotated
        pt1 = value[:2]
        pt2 = value[2:4]
        for j in range(4):
            # rotate the point pt2
            angle = j * (math.pi / 2)

            x22 = ((pt2[0] - pt1[0]) * math.cos(angle)) - ((pt2[1] - pt1[1]) * math.sin(angle)) + pt1[0]
            y22 = ((pt2[0] - pt1[0]) * math.sin(angle)) + ((pt2[1] - pt1[1]) * math.cos(angle)) + pt1[1]

            circle = tuple([int(x22), int(y22), int(average_radius)])

            # # debug print of the 90 degree check
            # cv2.circle(binary_img_copy, (pt1[0], pt1[1]), 2, (0, 255, 0), thickness=2)
            # cv2.line(binary_img_copy, tuple(pt1), tuple([int(x22), int(y22)]), (255, 0, 0), 2)
            # cv2.circle(binary_img_copy, tuple([int(x22), int(y22)]), int(average_radius), (255,0, 0),
            # thickness=2)
            # cv2.imshow("Origin points", binary_img_copy)
            # cv2.waitKey()

            # slower way how to check all conditions
            # condition1 = self.is_solderball(binary_img, circle, hist=0.2, ratio=25)
            # condition2 = self.is_circle_overlaping(circle, found_circles)
            # condition3 = self.is_circle_overlaping(circle, self.solderballs)
            #
            # if condition1 and not condition2 and not condition3:
            #     found_circles = np.append(found_circles, [circle], axis=0)
            #     cv2.circle(binary_img_copy, (circle[0],circle[1]), int(self.average_radius), (255, 0, 0),
            #                thickness=2)
            #     cv2.imshow("Origin points", binary_img_copy)
            #     cv2.waitKey()

            if is_circle_overlaping(circle, found_circles, average_radius):
                continue
            elif not (is_solderball(binary_img, circle, hist=0.2, ratio=25)):
                continue
            elif is_circle_overlaping(circle, solderballs, average_radius):
                continue
            else:
                found_circles = np.append(found_circles, [circle], axis=0)
                # cv2.circle(binary_img_copy, (circle[0],circle[1]), int(average_radius), (0, 0, 255),
                #            thickness=5)
                #
                # output.debug_show("Origin points", binary_img_copy,fxy=0.2,debug_mode=True)
                # cv2.waitKey()

    return found_circles


def is_circle_overlaping(circle, circle_array, average_radius):
    """
    It will check whereas is the input circle overlaping with any of the circle from the circle_array

    :param circle: inputh circle that will be compared with all circles in the circle_array
    :param circle_array: array of circles
    :return: boolean True - the circle is overlapping
                     False - circle is not overlapping
    """
    pt1 = np.array([[circle[0], circle[1]]])

    dist = np.sqrt((circle_array[:, 0] - pt1[0, 0]) ** 2 + (circle_array[:, 1] - pt1[0, 1]) ** 2)

    if len(dist[dist < (2 * average_radius)]) > 0:
        return True
    else:
        return False


def rect_contains(rect, point):
    """
    Tests if the bounding rectangle contains the point
    :param rect:
    :param point:
    :return:
    """
    if point[0] < rect[0]:
        return False
    elif point[1] < rect[1]:
        return False
    elif point[0] > rect[2]:
        return False
    elif point[1] > rect[3]:
        return False
    return True


def is_line_segment_intersection(pt_a, pt_b, pt_c, pt_d):
    """
    Decides if the two line segments are intersecting. First line is defined by points pt_a and pt_b and the second line
    is defined by points pt_c and pt_d. Points pt_a and pt_b will be "anchor" An intersection exists if, and only if,
    one of the points (pt_c or pt_d) is OnLeft and the other is OnRight.
    (source: http://stackoverflow.com/questions/3838329/how-can-i-check-if-two-segments-intersect)

    :param pt_a: First point of first line
    :param pt_b: Second point of first line
    :param pt_c: First point of second line
    :param pt_d: Second point of second line
    :return: True if there is intersection
    """

    if __is_on_left(pt_a, pt_b, pt_c) and __is_on_right(pt_a, pt_b, pt_d) and not (__is_collinear(pt_a, pt_b, pt_c)) \
            and not (__is_collinear(pt_a, pt_b, pt_d)):
        print("Left - Right")
        return True

    elif __is_on_right(pt_a, pt_b, pt_c) and __is_on_left(pt_a, pt_b, pt_d) and not (__is_collinear(pt_a, pt_b, pt_c)) \
            and not (__is_collinear(pt_a, pt_b, pt_d)):
        print("Left - Right")
        return True

    else:
        return False


def __is_on_left(pt_a, pt_b, pt_c):
    """
    supporting function to find out intersection of two lines
    :param pt_a:
    :param pt_b:
    :param pt_c:
    :return:
    """
    return __calculate_triagle_area(pt_a, pt_b, pt_c) > 0


def __is_on_right(pt_a, pt_b, pt_c):
    """
    supporting function to find out intersection of two lines
    :param pt_a:
    :param pt_b:
    :param pt_c:
    :return:
    """
    return __calculate_triagle_area(pt_a, pt_b, pt_c) < 0


def __is_collinear(pt_a, pt_b, pt_c):
    """
    supporting function to find out intersection of two lines
    :param pt_a:
    :param pt_b:
    :param pt_c:
    :return:
    """
    return __calculate_triagle_area(pt_a, pt_b, pt_c) == 0


def __calculate_triagle_area(pt_a, pt_b, pt_c):
    """
    helper function of "is_line_segment_intersection" that calculates area between the "anchor" and the test point
    :param pt_a: first point of anchor
    :param pt_b: second point of anchor
    :param pt_c: test pont
    :return: area
    """

    return int((pt_b[0] - pt_a[0]) * (pt_c[1] - pt_a[1]) - (pt_c[0] - pt_a[0]) * (pt_b[1] - pt_a[1]))


def is_rectangle_overlapping(rect1, rect2):
    """
    Function that will check if two rectengles are overlapping. It works only with rectangles that are NOT rotated
    :param rect1: rectangle in the format (center x, center y), (width, height), roation
    :param rect2: rectangle in the format (center x, center y), (width, height), roation
    :return: True if there is intersection
    """
    (x1, y1), (width1, height1), angle1 = rect1
    (x2, y2), (width2, height2), angle2 = rect2

    xmin1 = x1 - width1 / 2
    ymin1 = y1 - height1 / 2
    xmax1 = x1 + width1 / 2
    ymax1 = y1 + height1 / 2

    xmin2 = x2 - width2 / 2
    ymin2 = y2 - height2 / 2
    xmax2 = x2 + width2 / 2
    ymax2 = y2 + height2 / 2

    dx = min(xmax1, xmax2) - max(xmin1, xmin2)
    dy = min(ymax1, ymax2) - max(ymin1, ymin2)

    if (dx >= 0) and (dy >= 0):
        return True
    else:
        return False


def __ccw(pt1, pt2, pt3):
    return (pt3[1] - pt1[1]) * (pt2[0] - pt1[0]) > (pt2[1] - pt1[1]) * (pt3[0] - pt1[0])


def is_intersection(pt1, pt2, pt3, pt4):
    """
    Return true if line segments AB and CD intersect

    :param pt1:
    :param pt2:
    :param pt3:
    :param pt4:
    :return:
    """
    return __ccw(pt1, pt3, pt4) != __ccw(pt2, pt3, pt4) and __ccw(pt1, pt2, pt3) != __ccw(pt1, pt2, pt4)


def is_rot_rect_square(rect, edge_ratio=0.2):
    (x, y), (width, height), _ = rect
    rect_ratio = width/height

    if (rect_ratio > (1 - edge_ratio)) and (rect_ratio < (1 + edge_ratio)):
        return True
    else:
        return False






