"""

    Author: Martin Horak
    Python Version: 3.5
    OpenCV Version: 3.1.0
    Description: This file contains function that are connected with the image manipulation

"""

import sys
import cv2
import numpy as np
import configparser
import os

from Ultron.utilities import output
from Ultron.utilities import utilities
from Ultron.enums import AlignMethod
from Ultron.utilities import find_functions
from Ultron.utilities.utilities import timeit


def use_thresholding(gray_img, method=3, custom_thresh=125):
    """
    This function will use one of the thresholding method and returns a binary image. User can specify the
    threshold level value but in case of Otsu thresholding we pass 0 so the algorythm finds the best thresholding
    value

    :param custom_thresh:
    :param method: thresholding method is identified by number
        1: Global thresholding with the threshold value 127
        2: Otsu's thresholding without blurring
        3: Otsu's thresholding after Gaussian filtering

    :param gray_img: input gray scaled image

    :return: binary image

    """

    if method == 1:
        # global thresholding
        ret1, th1 = cv2.threshold(gray_img, custom_thresh, 255, cv2.THRESH_BINARY)

        return th1

    elif method == 2:
        # Otsu's thresholding
        ret2, th2 = cv2.threshold(gray_img, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

        return th2

    elif method == 3:
        # Otsu's thresholding after Gaussian filtering
        blur = cv2.GaussianBlur(gray_img, (5, 5), 0)
        ret3, th3 = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)

        # print("ret3 = ", ret3)

        return th3
    else:
        sys.exit("Wrong input method. Check the use_thresholding fuction.")


def __is_bevelled_edge(original_image, rect, similarity, debug_mode=False):
    """
    This function will check if the potential QFN package has bevelled edge using the template. This function can be
    modified to look for multple shapes of the center part of the package (center part = package without pins)
    :param rect: boundary rectangle that contains examined object
    :param similarity: thresholding value that is used to decide if the teplate shape and the tested shape are similar
    :param debug_mode: boolean
    :return: boolean
    """
    (x, y), (width, height), angle = rect

    # cut the cectengle from the original image
    img_copy = original_image.copy()

    # I am calculating half of the height because rotation rectangle is giving me centre of rectangle + I am
    # multiplying it with some constant to get small gap between the object and the bounding rectangle
    height_half = int((height / 2) * 1.0)
    width_half = int((width / 2) * 1.0)
    img_copy = img_copy[int(y) - height_half:int(y) + height_half, int(x) - width_half:int(x) + width_half]

    output.debug_show("Cropped", img_copy, waitkey=False, debug_mode=debug_mode)
    # ToDo: Find out how to eliminate the effect of interfering PCB
    # use enhancing or thresholding
    gray = cv2.cvtColor(img_copy, cv2.COLOR_BGR2GRAY)
    binary = use_thresholding(gray)

    # use closing
    kernel = np.ones((5, 5), np.uint8)
    closing = cv2.morphologyEx(binary, cv2.MORPH_CLOSE, kernel, iterations=5)
    img_test, contour_test, hierarchy_test = cv2.findContours(closing.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    output.debug_show("Binary", closing, waitkey=False, debug_mode=debug_mode)
    output.show_contours("Contour", img_copy, contour_test, waitkey=True, debug_mode=debug_mode)

    # compare with template/reference shape
    template = cv2.imread(
        '/Users/horczech/PycharmProjects/db_Project/Project Ultron/images/shape_template/template_QFN.png')
    gray = cv2.cvtColor(template, cv2.COLOR_BGR2GRAY)
    binary = use_thresholding(gray)

    # ToDo: Save the contour of the teplate to save some computation power
    img_temp, contour_temp, hierarchy_temp = cv2.findContours(binary, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
    # output.show_contours("Template", template, contour_temp, waitkey=False)

    # calculate the similarity of template shape and the test shape and if is lower than the "similarity" parametrer
    # it will be considered as a potential QFN package
    # PARAM: the lower the "similarity" parameter is the more are the compared contours similar
    for cont in contour_test:
        ret = cv2.matchShapes(cont, contour_temp[0], 1, 0.0)
        if debug_mode:
            print("Similarity: ", ret)

        if ret < similarity:
            return True

    return False


def __is_surrounded_by_pins(original_image, center_rectangle, inspecton_radius=1.9, intersections=3,
                            min_number_of_pins=-1,
                            debug_mode=False, fxy=1):
    """
    This function will check if the centre part of the QFN package is surrounded by symetrically placed pins

    :param center_rectangle: (x,y) coordinates
    :param inspecton_radius: constant that will define the size of the area around the QFN packege center that
    :param min_number_of_pins: user can specify min number of pins that should surround the qfn package. If you dont
                                want to use this parameter enter some negative number
    :param debug_mode: boolean
    :param fxy: resize factor
    :return: boolean
    """

    (x, y), (width, height), angle = center_rectangle

    # I am calculating half of the height because rotation rectangle is giving me centre of rectangle + I am
    # multiplying it with some constant to get small gap between the object and the bounding rectangle
    height_half = int((height / 2) * inspecton_radius)
    width_half = int((width / 2) * inspecton_radius)

    # crop the component + some area around that is defined with the "radius"
    img_cropped = original_image.copy()
    img_cropped = img_cropped[int(y) - height_half:int(y) + height_half, int(x) - width_half:int(x) + width_half]

    # if cropping goes wrong due to the getting behind the borders of the image
    if img_cropped.size == 0:
        return False

    # recalculate the possition of the center rectangle
    rect_after_cropping = ((img_cropped.shape[1] / 2, img_cropped.shape[0] / 2), (width, height), angle)

    # debug print
    output.debug_show("Cropped component", img_cropped, debug_mode=debug_mode, fxy=fxy, waitkey=False)

    # find all components (pins + center rectangle)
    closing, all_rectangles = find_functions.find_componets_boundary(img_cropped, k_size=5, iterations=5)

    img_debug = img_cropped.copy()
    if debug_mode:
        # draw lines from the centre of each pin to the centre of the QFN
        for rectangle in all_rectangles:
            (x2, y2), (_, _), angle2 = rectangle
            cv2.line(img_debug, (int(img_debug.shape[1] / 2), int(img_debug.shape[0] / 2)), (int(x2), int(y2)),
                     (0, 0, 255), 2)

    # debug print
    output.debug_show("After closing", closing, debug_mode, waitkey=False)
    output.debug_show_rot_rects("Intersections", img_debug, all_rectangles, debug_mode, waitkey=True)

    # filter out the rectangles that are overlapping the center rectangle of the QFN package
    boundaries_of_pins = []
    for rectangle in all_rectangles:
        if not (utilities.is_rectangle_overlapping(rectangle, rect_after_cropping)):
            boundaries_of_pins.append(rectangle)

    qfn_intersection_count = __calculate_pin_qfn_intersections(boundaries_of_pins, rect_after_cropping)

    if (qfn_intersection_count[0] > intersections and
                qfn_intersection_count[1] > intersections and
                qfn_intersection_count[2] > intersections and
                qfn_intersection_count[3] > intersections):

        if min_number_of_pins > 0 and sum(qfn_intersection_count) >= min_number_of_pins:
            # if user wants to check min number of pins and the test is possitive
            return True
        elif min_number_of_pins > 0 and sum(qfn_intersection_count) < min_number_of_pins:
            # if user wants to check min number of pins and the test is negative
            return False
        else:
            # if user doesnt want check minimum number of pins
            return True
    else:
        return False


def __calculate_pin_qfn_intersections(boundaries_of_pins, rect_after_cropping):
    """
    This function will get the pins that are around the qfn center rectangle and the center rectangle itself.
    It will calculate how many times does the lines that are connecting the center of the pin and the center
    of the qfn package cross the top,left, right or bottom border
    :param boundaries_of_pins: rotation rectangles of the pins that are in the neighborhood of the center part of
        the qfn package
    :param rect_after_cropping: the coordinates of the center part of the qfn package
    :return: array with four integers [bottom_line_intersection_count, right_line_intersection_count,
                              top_line_intersection_count, left_line_intersection_count] that represent number of
                              intersections of the lines with the particular border line of the qfn package
    """
    # count the intersections of top, bottom, left and right edge
    (x_qfn_cropped, y_qfn_cropped), (width_qfn_cropped, height_qfn_cropped), angle_qfn_cropped = rect_after_cropping
    bottom_left_corner = (x_qfn_cropped - width_qfn_cropped / 2, y_qfn_cropped + height_qfn_cropped / 2)
    bottom_right_corner = (x_qfn_cropped + width_qfn_cropped / 2, y_qfn_cropped + height_qfn_cropped / 2)
    top_left_corner = (x_qfn_cropped - width_qfn_cropped / 2, y_qfn_cropped - height_qfn_cropped / 2)
    top_right_corner = (x_qfn_cropped + width_qfn_cropped / 2, y_qfn_cropped - height_qfn_cropped / 2)
    bottom_line = (bottom_left_corner, bottom_right_corner)
    right_line = (bottom_right_corner, top_right_corner)
    top_line = (top_left_corner, top_right_corner)
    left_line = (bottom_left_corner, top_left_corner)
    bottom_line_intersection_count = 0
    right_line_intersection_count = 0
    top_line_intersection_count = 0
    left_line_intersection_count = 0
    qfn_border_lines = [bottom_line, right_line, top_line, left_line]
    qfn_intersection_count = [bottom_line_intersection_count, right_line_intersection_count,
                              top_line_intersection_count, left_line_intersection_count]
    for index, border_line in enumerate(qfn_border_lines):
        for pin in boundaries_of_pins:
            pin_pt = pin[0]
            qfn_center = rect_after_cropping[0]

            qfn_pt1, qfn_pt2 = border_line

            if utilities.is_intersection(pin_pt, qfn_center, qfn_pt1, qfn_pt2):
                qfn_intersection_count[index] += 1
    return qfn_intersection_count


def subtract(base_img, test_image, debug_mode=False, fxy=1):
    """
    This method will preprocess both base and test image and subtract them
    :param base_img: gray scale image
    :param fxy: resize factor
    :param debug_mode: boolean
    :param test_image: image that we want to compare againts the base image
    :return: gray scale image that is the result of the image subtraction
    """

    # substract images
    positive_img = cv2.subtract(base_img, test_image)
    negative_img = cv2.subtract(test_image, base_img)

    substraction_result = positive_img + negative_img

    output.debug_show("base-test 2", positive_img, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    output.debug_show("test-base 2", positive_img, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    output.debug_show("ADD", substraction_result, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # # use median filter
    # substracted_filtered = cv2.medianBlur(img_out1, 11)
    # kernel = np.ones((5, 5), np.uint8)
    # substracted_filtered = cv2.morphologyEx(substracted_filtered, cv2.MORPH_OPEN, kernel, iterations=3)
    #
    # thresholded_image = use_thresholding(substracted_filtered, method=3)
    # output.debug_show("After substraction and median", thresholded_image, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    #
    # # apply thresholding
    # blur = cv2.GaussianBlur(base_img_cropped, (5, 5), 0)
    # ret3, base_thresholded = cv2.threshold(blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    #
    # ret1, test_thresholded = cv2.threshold(aligned_img, ret3, 255, cv2.THRESH_BINARY)
    #
    # output.debug_show("base_thresholded", base_thresholded, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    # output.debug_show("test_thresholded", test_thresholded, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    #
    # xor_image = cv2.bitwise_xor(base_thresholded, test_thresholded)
    # output.debug_show("XOR", xor_image, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    #
    # # do some filtering on the XOR image
    #
    # kernel = np.ones((3, 3), np.uint8)
    # xor_image_filtered = cv2.morphologyEx(xor_image, cv2.MORPH_OPEN, kernel, iterations=1)
    # xor_image_filtered = cv2.medianBlur(xor_image_filtered, 9)
    #
    # output.debug_show("XOR after median", xor_image_filtered, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    return substraction_result

@timeit
def allign_images(base_img, test_img, method=AlignMethod.ECC, debug_mode=False, fxy=1.0):
    """

    :param method: what method to use for image alignment
                    method ECC: works very good, combination of rought alignment and the precice ECC method
                    method SIFT: doesnt work so good as ECC -- not recemended
    :param base_img: referencial image
    :param test_img: image that will be align according to the
    :param debug_mode: boolean
    :param fxy: resize factor
    :return:
    """

    if method == AlignMethod.SIFT:
        test_aligned = alignment_sift(base_img, debug_mode, fxy, test_img)

    elif method == AlignMethod.ECC:
        test_aligned = alignment_ecc(base_img, test_img, debug_mode=debug_mode, fxy=fxy)

    else:
        sys.exit("Wrong method !")

    output.debug_show("Aligned images", test_aligned, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    return test_aligned


def align_roughtly(iteration, base_image, test_image, threshold_value=100, fxy=1):
    """
    This function will get two images and the test image is rotated 4 times by 90 degrees and substracted. The angke
    that results in the image that is most similar to the base image is the result of rought alignment
    :param iteration: we have to turn the image 4 times by 90 degrees. Iteration is teling us how many degrees should
                      be the image turned
    :param base_image: referencial image
    :param test_image: image that will be aligned
    :param threshold_value: threshold value
    :param fxy: resize factor
    :return:
    """
    if iteration == 0:
        # no rotation
        rotated_image = test_image

        resized_img = cv2.resize(rotated_image, (base_image.shape[1], base_image.shape[0]))
        res1 = cv2.absdiff(base_image, resized_img)

        ret1, th1 = cv2.threshold(res1, threshold_value, 255, cv2.THRESH_BINARY)
        white_pixels_count = cv2.countNonZero(th1)
        # print(white_pixels_count)
    elif iteration == 1:
        # rotate 90 degrees CW
        rotated_image = cv2.transpose(test_image)
        rotated_image = cv2.flip(rotated_image, 1)

        rot_img_temp = cv2.resize(rotated_image, (base_image.shape[1], base_image.shape[0]))
        res2 = cv2.absdiff(base_image, rot_img_temp)

        ret1, th1 = cv2.threshold(res2, threshold_value, 255, cv2.THRESH_BINARY)
        white_pixels_count = cv2.countNonZero(th1)
        # print(white_pixels_count)

    elif iteration == 2:
        # rotate 90 degrees CCW
        rotated_image = cv2.transpose(test_image)
        rotated_image = cv2.flip(rotated_image, 0)

        rot_img_temp = cv2.resize(rotated_image, (base_image.shape[1], base_image.shape[0]))
        res3 = cv2.absdiff(base_image, rot_img_temp)
        ret1, th1 = cv2.threshold(res3, 100, 255, cv2.THRESH_BINARY)
        white_pixels_count = cv2.countNonZero(th1)
        # print(white_pixels_count)
    elif iteration == 3:
        rotated_image = cv2.flip(test_image, -1)

        rot_img_temp = cv2.resize(rotated_image, (base_image.shape[1], base_image.shape[0]))
        res4 = cv2.absdiff(base_image, rot_img_temp)
        ret1, th1 = cv2.threshold(res4, 100, 255, cv2.THRESH_BINARY)
        white_pixels_count = cv2.countNonZero(th1)
        # print(white_pixels_count)
    else:
        sys.exit("Error in rought alignemt")

    # print(white_pixels_count)
    # output.debug_show("WTF WTF WTF", rotated_image, debug_mode=True, fxy=fxy)

    return rotated_image, white_pixels_count


def alignment_ecc(base_img, test_img, debug_mode=False, fxy=1.0):
    """
    Implementation of the ECC method for image alignment
    :param base_img: referencial image
    :param test_img: image that will be aligned
    :param debug_mode: boolean
    :param fxy: boolean
    :return: aligned image
    """

    if len(base_img.shape) > 2:
        # Convert images to grayscale
        base_gray = cv2.cvtColor(base_img, cv2.COLOR_BGR2GRAY)
        test_gray = cv2.cvtColor(test_img, cv2.COLOR_BGR2GRAY)
    else:
        base_gray = base_img
        test_gray = test_img

    # do the rought alignment
    #  .........................................................................

    min_white_pizels_count = sys.maxsize
    rough_aligned_img = []
    for i in range(4):
        rotated_image, white_pixels_count = align_roughtly(i, base_gray, test_gray, threshold_value=100,fxy=fxy)

        if white_pixels_count < min_white_pizels_count:
            min_white_pizels_count = white_pixels_count
            rough_aligned_img = rotated_image
        else:
            continue

    test_gray = rough_aligned_img
    output.debug_show("Roughtly aligned image", test_gray, debug_mode=debug_mode,fxy=fxy, waitkey=True)

    #  .........................................................................

    # Find size of base_ image
    sz = base_gray.shape
    # Define the motion model
    # PARAM: you can try different modes
    # warp_mode = cv2.MOTION_TRANSLATION
    warp_mode = cv2.MOTION_EUCLIDEAN
    # Define 2x3 or 3x3 matrices and initialize the matrix to identity
    if warp_mode == cv2.MOTION_HOMOGRAPHY:
        warp_matrix = np.eye(3, 3, dtype=np.float32)
    else:
        warp_matrix = np.eye(2, 3, dtype=np.float32)

    # Specify the number of iterations.
    # number_of_iterations = 5000
    number_of_iterations = 100
    # Specify the threshold of the increment
    # in the correlation coefficient between two iterations
    # termination_eps = 1e-10
    termination_eps = 0.003
    # Define termination criteria
    criteria = (cv2.TERM_CRITERIA_EPS | cv2.TERM_CRITERIA_COUNT, number_of_iterations, termination_eps)
    # Run the ECC algorithm. The results are stored in warp_matrix.
    (cc, warp_matrix) = cv2.findTransformECC(base_gray, test_gray, warp_matrix, warp_mode, criteria)
    if warp_mode == cv2.MOTION_HOMOGRAPHY:
        # Use warpPerspective for Homography
        test_aligned = cv2.warpPerspective(test_gray, warp_matrix, (sz[1], sz[0]),
                                           flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)
    else:
        # Use warpAffine for Translation, Euclidean and Affine
        test_aligned = cv2.warpAffine(test_gray, warp_matrix, (sz[1], sz[0]),
                                      flags=cv2.INTER_LINEAR + cv2.WARP_INVERSE_MAP)
    return test_aligned


def alignment_sift(base_img, debug_mode, fxy, test_img):
    """
    Implementation of the image alignment using the SIFT features
    :param base_img: referential image
    :param debug_mode: boolean
    :param fxy: resize factor
    :param test_img: image that will be aligned
    :return: aligned image
    """
    sift = cv2.xfeatures2d.SIFT_create()
    kp1, des1 = sift.detectAndCompute(base_img, None)
    kp2, des2 = sift.detectAndCompute(test_img, None)
    img = cv2.drawKeypoints(base_img, kp1, base_img)
    img2 = cv2.drawKeypoints(test_img, kp2, test_img)
    output.debug_show("SIFT base image", img, debug_mode=debug_mode, fxy=fxy)
    output.debug_show("SIFT test image", img2, debug_mode=debug_mode, fxy=fxy)
    # BFMatcher with default params
    bf = cv2.BFMatcher()
    matches = bf.knnMatch(des1, des2, k=2)
    # Apply ratio test
    good = []
    for m, n in matches:
        if m.distance < 0.75 * n.distance:
            good.append([m])

    # sort the matches according to the distance and pick just top 50
    good_sorted = sorted(good, key=_get_key_sorting)
    best_matches = good_sorted[:100]
    # cv2.drawMatchesKnn expects list of lists as matches.
    img3 = cv2.drawMatchesKnn(base_img, kp1, test_img, kp2, best_matches, flags=2, outImg=base_img)
    output.debug_show("Matches", img3, debug_mode=debug_mode, fxy=fxy)
    # # debug print of each match
    # for index, match in enumerate(best_matches):
    #     base_color = cv2.cvtColor(base_img,cv2.COLOR_GRAY2BGR)
    #     test_color = cv2.cvtColor(test_img,cv2.COLOR_GRAY2BGR)
    #
    #     cv2.circle(base_color,tuple(np.int0(kp1[match[0].queryIdx].pt)),5,(0,0,255),5)
    #     cv2.circle(test_color, tuple(np.int0(kp2[match[0].trainIdx].pt)), 5, (0, 0, 255), 5)
    #
    #
    #     print("Distance: ", best_matches[index][0].distance)
    #     output.debug_show("base", base_color, debug_mode=debug_mode, fxy=0.4, waitkey=False)
    #     output.debug_show("tits", test_color, debug_mode=debug_mode, fxy=0.4)
    #     cv2.waitKey()
    base_keypoints1 = np.float32([kp1[m[0].queryIdx].pt for m in best_matches]).reshape(-1, 1, 2)
    test_keypoints1 = np.float32([kp2[m[0].trainIdx].pt for m in best_matches]).reshape(-1, 1, 2)
    # Calculate Homography
    h, status = cv2.findHomography(base_keypoints1, test_keypoints1)
    # Warp source image to destination based on homography
    im_out = cv2.warpPerspective(test_img, h, (base_img.shape[1], base_img.shape[0]))
    output.debug_show("After rotation", im_out, debug_mode=debug_mode, fxy=fxy)
    output.debug_show("Origo image", base_img, debug_mode=debug_mode, fxy=fxy)
    return im_out


def crop_pcb(input_image, pcb_thresholding=190, closing_kernel=5, closing_iterations=3, debug_mode=False, fxy=1):
    """

    :param closing_iterations: number of ittereations for morpjological operation
    :param closing_kernel: size of the kernel for the morphological operation
    :param pcb_thresholding: thresholding level for the detection of the edge of pcb board
    :param fxy: resize factor
    :param debug_mode: boolean
    :param input_image: grayscale image
    :return: cropped pcb image
    """
    # debug image
    input_image_color = cv2.cvtColor(input_image,cv2.COLOR_GRAY2BGR)

    # find components on the image
    component_closing_img, component_boundaries = find_functions.find_componets_boundary(input_image, k_size=5,
                                                                                         iterations=2)

    # debug print
    output.debug_show("Binary image components", component_closing_img, debug_mode=debug_mode, fxy=fxy,
                      waitkey=True)
    output.debug_show_rot_rects("Boundaru rectangles component", input_image_color.copy(), component_boundaries,
                                debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # find pcb on the image
    pcb_closing_img, pcb_boundaries = find_functions.find_pcb_boundary(input_image, k_size=closing_kernel,
                                                                       iterations=closing_iterations,
                                                                       thresholding=pcb_thresholding)

    output.debug_show("Binary image PCB outline", pcb_closing_img, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    output.debug_show_rot_rects("Boundary rectangles PCB", input_image_color.copy(), pcb_boundaries,
                                debug_mode=debug_mode, fxy=fxy, waitkey=True)

    try:
        pcb_boundary = filter_pcb_boundary(component_boundaries, pcb_boundaries)

    except ValueError:
        print("filter_pcb_boundary fuction probably didnt find any PCB boundary check the image or change the "
              "parameters of filter function")
        raise

    output.debug_show_rot_rects("Boundary of the PCB", input_image.copy(), [pcb_boundary],
                                debug_mode=debug_mode, fxy=fxy, waitkey=True)

    rotated_img, (x_min, x_max, y_min, y_max) = make_pcb_upright(input_image, pcb_boundary)
    rotated_img = rotated_img[y_min:y_max, x_min:x_max]

    output.debug_show("After making rotated boundary upright", rotated_img, fxy=fxy, debug_mode=debug_mode)

    return rotated_img


def make_pcb_upright(input_gray_img, pcb_boundary):
    """
    It will find a pcb on the image and rotates the image so the pcb board will be upright

    :param input_gray_img: image containing pcb board
    :param pcb_boundary: boundary of pcb board
    :return: rotated image
    """
    # grab the dimensions of the image and then determine the center
    (h, w) = input_gray_img.shape[:2]
    (x_pcb, y_pcb), _, angle_pcb = pcb_boundary
    (cX, cY) = (x_pcb, y_pcb)

    # grab the rotation matrix (applying the negative of the angle to rotate clockwise), then grab the sine
    # and cosine (i.e., the rotation components of the matrix)
    M = cv2.getRotationMatrix2D((cX, cY), angle_pcb, 1.0)
    cos = np.abs(M[0, 0])
    sin = np.abs(M[0, 1])

    # ToDo: The correction of the new image width and height doesnt work properly -- need to add translation to the
    # ToDo: rotation matrix that equals the shift between the image centre and the centre of the pcb
    # compute the new bounding dimensions of the image
    new_width = int((h * sin) + (w * cos))
    new_height = int((h * cos) + (w * sin))

    # adjust the rotation matrix to take into account translation
    M[0, 2] += (new_width / 2) - x_pcb
    M[1, 2] += (new_height / 2) - y_pcb

    # calculate the vertices the rotated image
    x1, y1 = rotate_point((0, 0), M)
    x2, y2 = rotate_point((w, 0), M)
    x3, y3 = rotate_point((w, h), M)
    x4, y4 = rotate_point((0, h), M)

    if x1 > new_width or x2 > new_width or x3 > new_width or x4 > new_width:
        new_width = max(x1, x2, x3, x4)
    if y1 > new_height or y2 > new_height or y3 > new_height or y4 > new_height:
        new_height = max(y1, y2, y3, y4)

    rotated_image = cv2.warpAffine(input_gray_img, M, (new_width, new_height))

    box = cv2.boxPoints(pcb_boundary)
    # calculate the vertices the pcb boundary
    pcb_x1, pcb_y1 = rotate_point(box[0], M)
    pcb_x2, pcb_y2 = rotate_point(box[1], M)
    pcb_x3, pcb_y3 = rotate_point(box[2], M)
    pcb_x4, pcb_y4 = rotate_point(box[3], M)

    x_min = int(min(pcb_x1, pcb_x2, pcb_x3, pcb_x4))
    x_max = int(max(pcb_x1, pcb_x2, pcb_x3, pcb_x4))

    y_min = int(min(pcb_y1, pcb_y2, pcb_y3, pcb_y4))
    y_max = int(max(pcb_y1, pcb_y2, pcb_y3, pcb_y4))

    return cv2.warpAffine(input_gray_img, M, (new_width, new_height)), (x_min, x_max, y_min, y_max)


def filter_pcb_boundary(component_boundaries, pcb_boundaries):
    """
    It will get all potential pcb boundaries and returns the true pcb boundary
    :param component_boundaries: array of rotation rectangles representing components of inside the pcb board
                                ((x,y),(width, height),angle)
    :param pcb_boundaries: array of rotation rectangles representing condidates for the pcb boundary
                         ((x,y),(width, height),angle)
    :return: rotation rectangle of the pcb board
    """
    # browse every PCB rectangle and pick the one that surrounds most of the component rectangles
    max_component = 0
    pcb_boundary = None
    for index, potential_pcb_boundary in enumerate(pcb_boundaries):
        (x_pcb, y_pcb), (width_pcb, height_pcb), angle_pcb = potential_pcb_boundary
        component_count = 0

        x_min = x_pcb - width_pcb / 2
        x_max = x_pcb + width_pcb / 2
        y_min = y_pcb - height_pcb / 2
        y_max = y_pcb + height_pcb / 2

        for component_boundary in component_boundaries:
            (x_component, y_component), (width_component, height_component), angle_component = component_boundary

            if x_min < x_component < x_max and y_min < y_component < y_max:
                component_count += 1
        if component_count > max_component:
            max_component = component_count
            pcb_boundary = potential_pcb_boundary

    if pcb_boundary is None:
        raise ValueError("No PCB boards were found. Check the image or the filtering parameters of "
                         "functions that are supposed to find PCB boundaries or component boundaries. ")

    return pcb_boundary


def rotate_point(pt, rotation_matrix):
    """
    rotate point using the rotation matrix
    :param pt: (x,y)
    :param rotation_matrix:
    :return: new coordinates of the x and y
    """
    x_new = int(rotation_matrix[0, 0] * pt[0] + rotation_matrix[0, 1] * pt[1] + rotation_matrix[0, 2])
    y_new = int(rotation_matrix[1, 0] * pt[0] + rotation_matrix[1, 1] * pt[1] + rotation_matrix[1, 2])

    return x_new, y_new


def _get_key_sorting(item):
    """supporting function for the sorting
    item: variable that will affect the sorting
    """
    return item[0].distance


# def crop_and_align_images(base_image, test_image, align_method=AlignMethod.ECC,
#                           crop_closing_kernel=5, crop_closing_iterations=3, debug_mode=False,
#                           fxy=1):

def crop_and_align_images(base_image, test_image,
                          configuration,
                          align_method=AlignMethod.ECC,
                          debug_mode=False,
                          fxy=1):
    """
    Function that will do both cropping and alignment of the image
    :param configuration: configuration file that contains all following variables
        crop_closing_kernel:kernel size for the kernel for morphological closing
        crop_closing_iterations: number of iterations
    :param base_image: color image
    :param test_image: color image
    :param debug_mode: boolean
    :param fxy: resize factor
    :return: both images that are cropped and aligned ready for image subtraction
    """
    # check whether the config file exists
    try:
        if not os.path.isfile(configuration):
            raise IOError('The entered path of the configuration file does not exists.')
    except IOError:
        print('Entered path: ', configuration)
        raise

    config = configparser.ConfigParser()
    config.read(configuration)
    try:
        if config.has_section('crop_and_align_images'):

            crop_closing_kernel = int(config['crop_and_align_images']['crop_closing_kernel'])
            crop_closing_iterations = int(config['crop_and_align_images']['crop_closing_iterations'])

        else:
            raise ValueError('The configuration file must contain section: crop_and_align_images')
    except KeyError:
        print('One of the following variables is missing in the configuration file: '
              'crop_closing_kernel, crop_closing_iterations')
        raise
    except ValueError:
        raise


    # Convert it to grayscale
    base_img_gray = cv2.cvtColor(base_image, cv2.COLOR_BGR2GRAY)
    test_img_gray = cv2.cvtColor(test_image, cv2.COLOR_BGR2GRAY)

    # Crop the PCB boards
    base_img_cropped = crop_pcb(base_img_gray, debug_mode=debug_mode, fxy=fxy, pcb_thresholding=180,
                                closing_kernel=crop_closing_kernel, closing_iterations=crop_closing_iterations)
    test_img_cropped = crop_pcb(test_img_gray, debug_mode=debug_mode, fxy=fxy, pcb_thresholding=180,
                                closing_kernel=crop_closing_kernel, closing_iterations=crop_closing_iterations)

    # Align images
    test_image_aligned = allign_images(base_img_cropped, test_img_cropped, method=align_method,
                                       debug_mode=debug_mode, fxy=fxy)

    return base_img_cropped, test_image_aligned
