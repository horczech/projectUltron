"""

    Author: Martin Horak
    Python Version: 3.5
    OpenCV Version: 3.1.0
    Description: Functions that are used to segment some objects from the xray image of PCB

"""

import sys
import cv2
import numpy as np
import os

from Ultron.utilities import output
from Ultron.utilities import utilities
from Ultron.utilities import image_operations
from Ultron.component.comp_solderbal import SolderBalls
from Ultron.component.comp_qnf_package import QFNPackage
from Ultron.enums import CircleDetectionMethod
import configparser


def find_solderballs(input_image,
                     configuration,
                     method=CircleDetectionMethod.BoundaryRectangles,
                     fxy=0.7,
                     debug_mode=False
                     ):
    """
    This function will segment all solder balls and returns array of SolderBall objects

    :param configuration: configuration file that contains all following variables
            intensity_ratio: percentage of white pixels that can be inside the solderball. It will be used to filter out
                            false detected solderballs
            radius_hysteresis: it will be used to filter out too big or too small circles
            closing_iterations: number of itteration of morphological closing
            closing_kernel_size: size of the kernel of the morphological closing
    :param input_image: color image of the pcb board that contains BGA
    :param debug_mode: if True all the redundant images and paramaters will be print
    :param fxy: factor of resizing of the image. It will be used on both axes
    :param method: You can pick from several methods to find a solder balls on the image
                    method = BoundaryRectangles: Uses boundary rectangles to find the circles on the image
                    method = HoughtCircles: Uses Hought transformation to find circles on the image
    :return: array of SolderBall objects
    """

    # check whether the config file exists
    try:
        if not os.path.isfile(configuration):
            raise IOError('The entered path of the configuration file does not exists.')
    except IOError:
        print('Entered path: ', configuration)
        raise

    config = configparser.ConfigParser()
    config.read(configuration)
    try:
        if config.has_section('find_solderballs'):
            closing_kernel_size = tuple([int(i) for i in config['find_solderballs']['closing_kernel_size'].split(',')])
            closing_iterations = int(config['find_solderballs']['closing_iterations'])
            radius_hysteresis = float(config['find_solderballs']['radius_hysteresis'])
            intensity_ratio = float(config['find_solderballs']['intensity_ratio'])
            use_triangulation = bool(config['find_solderballs']['use_triangulation'])
        else:
            raise ValueError('The configuration file must contain section: find_solderballs')
    except KeyError:
        print('One of the following variables is missing in the configuration file: '
              'closing_kernel_size, closing_iterations, radius_hysteresis, intensity_ratio, use_triangulation')
        raise
    except ValueError:
        raise

    solderballCount = -1
    average_radius = -1
    # n x 3 array containing x,y coordinates and radius, ratio (void/ball_area) in percentage, contours, hierarchy
    solderballs = np.empty((0, 3), dtype=int)

    # convert to the grayscale
    gray = cv2.cvtColor(input_image, cv2.COLOR_BGR2GRAY)

    # make a binary image using the threshold
    binary_img = image_operations.use_thresholding(gray, method=3)

    # debug print
    output.debug_show("Binary image", binary_img, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # PARAM: Size of kernel and number of itteration will affect the binary image
    # use closing
    kernel = np.ones((closing_kernel_size), np.uint8)
    binary_img = cv2.morphologyEx(binary_img, cv2.MORPH_CLOSE, kernel, iterations=closing_iterations)

    # debug print
    output.debug_show("Binary image after closing", binary_img, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    try:
        # Detect circles
        if int(method.value) == 1:
            circles = utilities.get_hough_circles(binary_img, fxy=fxy)
        elif int(method.value) == 2:
            circles = utilities.get_contour_circles(binary_img)
        else:
            raise ValueError("User entered wrong method.")
    except AttributeError:
        raise

    try:
        if (circles is None) or not (circles.any()):
            sys.exit("No circles found !")

    except SystemExit:
        print('SystemExit')
        raise

    # debug print
    binary_color_temp = cv2.cvtColor(binary_img, cv2.COLOR_GRAY2BGR)
    output.draw_circles("All detected circles", input_image, circles, debug_mode=debug_mode, fxy=fxy)

    # filter out circles that are wrong
    print("Circle count before filtering: ", circles.shape[0])
    for circle in circles:
        if utilities.is_solderball(binary_img, circle):
            solderballs = np.append(solderballs, [circle], axis=0)

    try:
        if (solderballs is None) or not (solderballs.any()):
            sys.exit("No circles found !")

    except SystemExit:
        raise

    # calculate the median of the radius of all circles the we found.
    # I assume that the majority of balls will be the right
    average_radius = np.median(solderballs[:, 2])

    # PARAM: histeresis of radius
    radius_hist = radius_hysteresis
    # delete all circles that are bigger than average radius
    solderballs = solderballs[(solderballs[:, 2] >= (1 - radius_hist) * average_radius) &
                              (solderballs[:, 2] <= (1 + radius_hist) * average_radius), :]

    try:
        if (solderballs is None) or not (solderballs.any()):
            sys.exit("No circles found !")

    except SystemExit:
        raise

    # update the numbers
    solderball_count = solderballs.shape[0]
    average_radius = np.median(solderballs[:, 2])

    print("Circle count after filtering: ", solderball_count)
    print("\n...........................\n")

    # debug print
    binary_color_img = cv2.cvtColor(binary_img, cv2.COLOR_GRAY2BGR)
    output.draw_circles("Filtered detected circles", binary_color_img, solderballs, debug_mode=debug_mode, fxy=fxy)

    count = 0

    # Todo: Make it fastert by browsing browsing each cintre of circle instead of edgeList (if its possible) or
    # Todo: by calculating average distance between the colder balls and browse the manualy

    if use_triangulation:
        while True:
            # Use the Delaunay Triangulation
            subdiv = utilities.delaunay_triangulation(solderballs[:, :2], input_image.shape)
            output.draw_delaunay("Delaunay Triangulation", binary_color_img, subdiv, debug_mode=debug_mode, fxy=fxy)

            # Identify missing circles using the 90 degrees info
            missing_circles2 = utilities.get_missing_circles2(subdiv, binary_img, solderballs, average_radius,
                                                              input_image.shape, ratio=intensity_ratio)
            solderballs = np.append(solderballs, missing_circles2, axis=0)

            # if not(missing_circles.any()) and not(missing_circles2.any()):
            if not (missing_circles2.any()):
                # if there are no new balls end the loop
                break
            elif count > 5:
                # somethong is wrong
                sys.exit("Triangulation did too much loops. Something is wromg.")
            else:
                print("Circle count after triangulation ", count, ": ", solderballs.shape[0])
                count += 1
                continue

        output.draw_delaunay("Delaunay Triangulation", binary_color_img, subdiv, debug_mode=debug_mode, fxy=fxy)

    # if debug_mode:
    #     # Draw Delaunay triangles
    #     utilities.draw_delaunay(binary_color_img, subdiv, (255, 0, 0))
    #     cv2.imshow("Delaunay Triangulation", binary_color_img)
    #     cv2.waitKey()

    output.draw_circles("After Delaunay Triangulation", binary_color_img, solderballs, debug_mode=debug_mode,
                        fxy=fxy)

    # ToDo: allign the centres of solderballs (Image gradiend calculation with the kernel size 2xradius) or by using contours

    # Todo: determinate if the solder ball is interfering with come component on the other side

    # create array of SolderBall objects

    # return SolderBalls(solderballs)

    return SolderBalls(solderballs)


def find_qnf_package(original_image,
                     configuration,
                     debug_mode=False,
                     fxy=1):
    """
    This function will get the xray image and finds all components with the QNF package.
    :param configuration: configuration file that contains all following variables
        min_number_of_pins:
        min_intersection_count:
        number of iterations of the morphological operation
        k_size: size of the kernel of the morphological operation
        is_bevelled_edge: Boolean value that will decide if the program shoul find qfn packages with bevelled edge
        is_square_package: Boolean value that will decide if the program shoul find qfn packages with square shape
    :param debug_mode: if True all the redundant images and paramaters will be print
    :param fxy: factor of resizing of the image. It will be used on both axes

    :param original_image: color image of pcb board
    :return: array of QFNPackage objects
    """

    # check whether the config file exists
    try:
        if not os.path.isfile(configuration):
            raise IOError('The entered path of the configuration file does not exists.')
    except IOError:
        print('Entered path: ', configuration)
        raise

    config = configparser.ConfigParser()
    config.read(configuration)
    try:
        if config.has_section('find_qnf_package'):
            is_square_package = bool(config['find_qnf_package']['is_square_package'])
            is_bevelled_edge = bool(config['find_qnf_package']['is_bevelled_edge'])
            k_size = int(config['find_qnf_package']['k_size'])
            min_intersection_count = int(config['find_qnf_package']['min_intersection_count'])
            iterations = int(config['find_qnf_package']['iterations'])
            min_number_of_pins = int(config['find_qnf_package']['min_number_of_pins'])

        else:
            raise ValueError('The configuration file must contain section: find_qnf_package')
    except KeyError:
        print('One of the following variables is missing in the configuration file: '
              'is_square_package, is_bevelled_edge, k_size, min_intersection_count, iterations')
        raise
    except ValueError:
        raise

    filtered_rects = []
    #  find big  components (component is a defined as big absorbing object)
    try:
        binary_big, rot_rects_big = find_componets_boundary(original_image, k_size, iterations)
    except SystemExit:
        print(
            'No component found. Check the input image or the input arguments of the find_componets_boundary function')
        raise

    # debug print
    output.debug_show("Binary image big components", binary_big, debug_mode, fxy=fxy)
    output.debug_show_rot_rects("Rectangle", original_image.copy(), rot_rects_big, debug_mode, fxy=fxy)

    # ToDo: Add symety check aka totate the pin around the center of the QFN package anc check if it is overlapping with some other pin
    qfn_packages = []
    for rect in rot_rects_big:
        # check if the component has square shape
        if is_square_package:
            condition_square_package = utilities.is_rot_rect_square(rect, edge_ratio=0.2)
        else:
            condition_square_package = True

        if is_bevelled_edge:
            condition_bevelled_edge = image_operations.__is_bevelled_edge(original_image, rect, similarity=0.05,
                                                                          debug_mode=False)
        else:
            condition_bevelled_edge = True

        condition_surrounded_by_pins = image_operations.__is_surrounded_by_pins(original_image, rect,
                                                                                inspecton_radius=1.9,
                                                                                intersections=min_intersection_count,
                                                                                min_number_of_pins=min_number_of_pins,
                                                                                debug_mode=debug_mode)

        if condition_bevelled_edge and condition_square_package and condition_surrounded_by_pins:
            qfn_packages.append(rect)

    if len(qfn_packages) == 0:
        sys.exit("No package found on the image. Please check the input image or the arguments of the find function")
    # debug print
    output.debug_show_rot_rects("QFN", original_image.copy(), qfn_packages, debug_mode, fxy=fxy)

    # return QFNPackage(qfn_packages)
    return QFNPackage(qfn_packages)


def find_componets_boundary(image, k_size=11, iterations=11):
    """

    :param image: input image
    :param k_size: size of the kernel of the morphological operation
    :param iterations: number of iterations of the morphological operation
    :return: boundary ractangles that surrounds potential components on the image and the image after morphological
             operation
    """

    if len(image.shape) > 2:
        # convert to gray scale
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    else:
        gray = image

    # use OTSU thresholding
    binary_img = image_operations.use_thresholding(gray, method=3)

    # use morphological closing that will filter out small objects including BGA
    kernel = np.ones((k_size, k_size), np.uint8)
    closing = cv2.morphologyEx(binary_img, cv2.MORPH_CLOSE, kernel, iterations=iterations)

    # find contours
    im2, contours, hierarchy = cv2.findContours(closing.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    # get rotated rectangles
    rot_rect = []
    for index, cnt in enumerate(contours):
        # I want to get rid of the outer contour that is surrounding whole image -> contour that doesnt have any
        # parrent
        if hierarchy[0][index, 3] != -1:
            rot_rect.append(cv2.minAreaRect(cnt))

    return closing, rot_rect


def find_componets_boundary_v2(image, k_size=11, iterations=11, debug_mode=True, fxy=1):
    """

    :param image: input image
    :param k_size: size of the kernel of the morphological operation
    :param iterations: number of iterations of the morphological operation
    :return: boundary ractangles that surrounds potential components on the image and the image after morphological
             operation
    """

    if len(image.shape) > 2:
        # convert to gray scale
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    else:
        gray = image

    # use OTSU thresholding
    binary_img = image_operations.use_thresholding(gray, method=3)

    # use morphological closing that will filter out small objects including BGA
    kernel = np.ones((k_size, k_size), np.uint8)
    closing = cv2.morphologyEx(binary_img, cv2.MORPH_CLOSE, kernel, iterations=iterations)

    # find contours
    im2, contours, hierarchy = cv2.findContours(closing.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    # image_color =cv2.cvtColor(gray, cv2.COLOR_GRAY2BGR)
    # output.show_contours("Component contours", image_color,contours,debug_mode=debug_mode, fxy=1)

    # get rotated rectangles
    rot_rect = []
    contours
    for index, cnt in enumerate(contours):
        # I want to get rid of the outer contour that is surrounding whole image -> contour that doesnt have any
        # parrent
        if hierarchy[0][index, 3] != -1:
            rot_rect.append(cv2.minAreaRect(cnt))

    return closing, rot_rect


def find_pcb_boundary(input_image, k_size=5, iterations=11, thresholding=170):
    """
    :param input_image: image of the pcb board
    :param k_size: size of the kernel of the morphological functions
    :param iterations: number of iterations of the morphological functions
    :param thresholding: thresholding level for image binarization
    :return: boundary rectangle of the pcb and the image after image processing
    """
    if len(input_image.shape) > 2:
        # convert to gray scale
        gray = cv2.cvtColor(input_image, cv2.COLOR_BGR2GRAY)
    else:
        gray = input_image

    # use thresholding
    binary_img = image_operations.use_thresholding(gray, method=1, custom_thresh=thresholding)

    # use opening
    kernel = np.ones((k_size, k_size), np.uint8)
    opening_img = cv2.morphologyEx(binary_img, cv2.MORPH_OPEN, kernel, iterations=iterations)

    # find contours
    im2, contours, hierarchy = cv2.findContours(opening_img.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    # get rotated rectangles
    rot_rect = []
    for index, cnt in enumerate(contours):
        # I want to get rid of the outer contour that is surrounding whole image -> contour that doesnt have any
        # parrent
        if hierarchy[0][index, 3] != -1:
            rot_rect.append(cv2.minAreaRect(cnt))

    return opening_img, rot_rect


def find_diferences_example(base_img, test_img, debug_mode, fxy):
    """
    Function that contains various operations that can be done with two aligned images.

    :param base_img: cropped and aligned referencial image
    :param test_img:  cropped and aligned tested image
    :param debug_mode: boolean
    :param fxy: factor for resizing of the image
    :return: None
    """

    # find components boundaries on the base image
    # base_closing_img, base_component_boundaries = find_componets_boundary(base_img, k_size=5, iterations=1)
    #
    # output.debug_show_rot_rects("Components on the base image", base_closing_img.copy(),base_component_boundaries,
    #                             debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # input images load and threshold
    base_blur = cv2.GaussianBlur(base_img, (5, 5), 0)
    thresh_value, base_bin = cv2.threshold(base_blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    test_bin = image_operations.use_thresholding(test_img, method=1, custom_thresh=thresh_value)

    output.debug_show("base image bin", base_bin, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    output.debug_show("test image bin", test_bin, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # complement images
    base_complement = cv2.bitwise_not(base_bin)
    test_complement = cv2.bitwise_not(test_bin)

    output.debug_show("base image complemented", base_complement, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    output.debug_show("test image complemented", test_complement, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # substract images
    negative_img = cv2.subtract(base_img, test_img)
    positive_img = cv2.subtract(test_img, base_img)
    substract_img = positive_img + negative_img

    output.debug_show("positive image", positive_img, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    output.debug_show("negative image", negative_img, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    output.debug_show("ADD", substract_img, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # threshold images
    positive_bin = image_operations.use_thresholding(positive_img, method=3)
    negative_bin = image_operations.use_thresholding(negative_img, method=3)
    substract_bin = image_operations.use_thresholding(substract_img, method=3)

    output.debug_show("positive bin", positive_bin, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    output.debug_show("negative bin", negative_bin, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    output.debug_show("ADD bin", substract_bin, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # flood fill
    # Mask used to flood filling.
    h, w = base_img.shape[:2]
    base_mask = np.zeros((h + 2, w + 2), np.uint8)

    h, w = test_img.shape[:2]
    test_mask = np.zeros((h + 2, w + 2), np.uint8)

    _, base_floodfill, _, _ = cv2.floodFill(base_complement, base_mask, (0, 0), 255)
    _, test_floodfill, _, _ = cv2.floodFill(test_complement, test_mask, (5, 5), 255)

    output.debug_show("base image complemented floodfill", base_floodfill, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    output.debug_show("test image complemented floodfill", test_floodfill, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # ----- ---- -- - - -- - -group 1 defect
    # Idh = cv2.subtract(base_floodfill, negative_bin)
    # output.debug_show("step1", Idh, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    #
    # Ig1 = cv2.subtract(Idh, negative_bin)
    # output.debug_show("step2", Ig1, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    #
    # Ig2 = cv2.subtract(negative_bin, Ig1)
    #
    # output.debug_show("GROUP1", Ig2, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # ----- ---- -- - - -- - -group 2 defect
    # Ide = cv2.subtract(test_floodfill,positive_bin)
    # Ig3 = cv2.subtract(test_floodfill,Ide)
    # output.debug_show("GROUP2", Ig3, debug_mode=debug_mode, fxy=fxy, waitkey=True)


    # ----- ---- -- - - -- - -group 3 defect
    # ----- ---- -- - - -- - -group 4 defect

    return None
