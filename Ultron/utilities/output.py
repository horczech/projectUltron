"""

    Author: Martin Horak
    Python Version: 3.5
    OpenCV Version: 3.1.0
    Description: This file contains functions to visualize results of the image processing.

"""

import cv2
import numpy as np
from Ultron.utilities import utilities
from Ultron.utilities import image_operations


def draw_circular_voids(img, circle, ratio, contours, hierarchy, error_thresh):
    """
    This finction draws the circle and the voids to the input image

    :param error_thresh:
    :param img: image where it will draw the voids and circles
    :param circle: input array containing [x,y,radius]
    :param ratio: ratio between void area and ball area
    :param contours: calculted contours
    :param hierarchy: calculted hierarchy
    :return: None
    """
    # visualize that the voids
    x_coordinate = circle[0]
    y_coordinate = circle[1]
    radius = circle[2]
    crop_raw = img[y_coordinate - radius:y_coordinate + radius, x_coordinate - radius:x_coordinate + radius, :]


    if len(contours) == 1:
        text = "Ratio: " + '0' "%"
        cv2.circle(img, (circle[0], circle[1]), circle[2], (0, 255, 0), thickness=1)
        cv2.putText(img, text, (int(circle[0] - circle[2]), int(circle[1] - circle[2] - 2)), cv2.FONT_HERSHEY_PLAIN,
                    fontScale=0.5, color=(0, 255, 0))
        return

    for index, contour in enumerate(contours):

        if hierarchy[0, index, 2] == -1:
            cv2.drawContours(crop_raw, contours, index, (0, 255, 0))

        if ratio > error_thresh:
            text = "Ratio: " + str(int(ratio)) + "%"
            cv2.circle(img, (circle[0], circle[1]), circle[2], (0, 0, 255), thickness=1)
            cv2.putText(img, text, (int(circle[0] - circle[2]), int(circle[1] - circle[2] - 2)), cv2.FONT_HERSHEY_PLAIN,
                        fontScale=0.5, color=(0, 0, 255))

        elif ratio == -1:
            text = "ERROR"
            cv2.circle(img, (circle[0], circle[1]), circle[2], (0, 0, 255), thickness=1)
            cv2.putText(img, text, (int(circle[0] - circle[2]), int(circle[1] - circle[2] - 2)), cv2.FONT_HERSHEY_PLAIN,
                        fontScale=0.5, color=(0, 0, 255))

        else:
            text = "Ratio: " + str(int(ratio)) + "%"
            cv2.circle(img, (circle[0], circle[1]), circle[2], (0, 255, 0), thickness=1)
            cv2.putText(img, text, (int(circle[0] - circle[2]), int(circle[1] - circle[2] - 2)), cv2.FONT_HERSHEY_PLAIN,
                        fontScale=0.5, color=(0, 255, 0))


def debug_show(windows_name, img, debug_mode=True, fxy=1, waitkey=True):
    """
    Print the image
    :param windows_name:
    :param img: input image that will be shown
    :param debug_mode: boolean
    :param fxy: resize factor
    :param waitkey: boolean
    :return: None
    """
    if debug_mode:

        # resize
        resized = cv2.resize(img, (0, 0), fx=fxy, fy=fxy, interpolation=cv2.INTER_LINEAR)

        cv2.imshow(windows_name, resized)

        if waitkey:
            cv2.waitKey()
        else:
            pass

    else:
        pass


def debug_show_rot_rects(windows_name, img, rot_rects, debug_mode=True, fxy=1, waitkey=True, color=(0, 0, 255)):
    """
    Function will visualize rotation rectangles and the image
    :param windows_name:
    :param img: input image
    :param rot_rects: boundary rectangles
    :param debug_mode: boolean
    :param fxy: resize factor
    :param waitkey: boolean
    :param color:
    :return: none
    """
    if debug_mode:
        # resize
        resized = cv2.resize(img, (0, 0), fx=fxy, fy=fxy, interpolation=cv2.INTER_LINEAR)

        # draw rectangles
        for rect in rot_rects:
            box = cv2.boxPoints(rect)
            box = np.int0(box * fxy)
            cv2.drawContours(resized, [box], 0, color, 1)

        cv2.imshow(windows_name, resized)

        if waitkey:
            cv2.waitKey()
        else:
            pass

    else:
        pass


def show_contours(windows_name, img, contours, debug_mode=True, fxy=1, waitkey=True, thickness=3):
    """
    Visualize contours
    :param windows_name:
    :param img: input image
    :param contours:
    :param debug_mode: boolean
    :param fxy: resize factor
    :param waitkey: boolean
    :param thickness: thickness of the conotur
    :return: None
    """
    if debug_mode:

        # draw rectangles
        cv2.drawContours(img, contours, -1, (0, 0, 255), thickness=thickness)

        # resize
        resized = cv2.resize(img, (0, 0), fx=fxy, fy=fxy, interpolation=cv2.INTER_LINEAR)

        cv2.imshow(windows_name, resized)

        if waitkey:
            cv2.waitKey()
        else:
            pass

    else:
        pass


def show_resized_contours(windows_name, img, contours, debug_mode=True, fxy=1, waitkey=True, thickness=3):
    """
    resize and visualize contours
    :param windows_name:
    :param img:  input image
    :param contours:
    :param debug_mode: boolean
    :param fxy: resize factor
    :param waitkey: boolean
    :param thickness: thickness of the conntour line
    :return:
    """
    if debug_mode:

        # resize
        resized = cv2.resize(img, (0, 0), fx=fxy, fy=fxy, interpolation=cv2.INTER_LINEAR)

        # resize contours
        contours_resized = [np.rint(cnt * fxy).astype(int) for cnt in contours]

        # draw contours
        cv2.drawContours(resized, contours_resized, -1, (0, 0, 255), thickness=thickness)

        cv2.imshow(windows_name, resized)

        if waitkey:
            cv2.waitKey()
        else:
            pass

    else:
        pass


def show_multiple_contours(windows_name, img, contours_list, colors, debug_mode=True, fxy=1, waitkey=True, thickness=3):
    """
    Show multiple conoturs
    :param windows_name:
    :param img: input image
    :param contours_list: list of conoturs
    :param colors:
    :param debug_mode: boolean
    :param fxy: resize factor
    :param waitkey: boolean
    :param thickness: thickness of the conotur line
    :return:
    """
    if debug_mode:

        # resize image
        resized = cv2.resize(img, (0, 0), fx=fxy, fy=fxy, interpolation=cv2.INTER_LINEAR)

        for index, contours in enumerate(contours_list):
            # resize contours
            contours_resized = [np.rint(cnt * fxy).astype(int) for cnt in contours]

            # draw contours
            cv2.drawContours(resized, contours_resized, -1, colors[index], thickness=thickness)

        cv2.imshow(windows_name, resized)

        if waitkey:
            cv2.waitKey()
        else:
            pass

    else:
        pass


def draw_circles(window_name, img, circles, debug_mode=True, fxy=1, wait_key=True, centre_color=(0, 0, 255),
                 circle_color=(0, 255, 0)):
    """
    visualuze circles
    :param window_name:
    :param img: input image
    :param circles:
    :param debug_mode: boolean
    :param fxy: resize factor
    :param wait_key: boolean
    :param centre_color: color of the center point
    :param circle_color: color of the circle
    :return:  none
    """
    if debug_mode:
        # resize
        resized = cv2.resize(img, (0, 0), fx=fxy, fy=fxy, interpolation=cv2.INTER_LINEAR)

        for circle in circles:
            circle = np.int0(circle * fxy)
            # draw the outer circle
            cv2.circle(resized, (circle[0], circle[1]), circle[2], circle_color, 2)
            # draw the center of the circle
            cv2.circle(resized, (circle[0], circle[1]), 2, centre_color, 2)

        cv2.imshow(window_name, resized)
        if wait_key:
            cv2.waitKey()


def draw_delaunay(window_name, img, subdiv, debug_mode=True, fxy=1, wait_key=True, delaunay_color=(255, 0, 0)):
    """
    Visualize delaunay triangles
    :param window_name:
    :param img: input image
    :param subdiv:
    :param debug_mode: boolean
    :param fxy: resize factor
    :param wait_key: boolean
    :param delaunay_color: color of the lines
    :return: None
    """
    if debug_mode:
        # resize
        resized = cv2.resize(img, (0, 0), fx=fxy, fy=fxy, interpolation=cv2.INTER_LINEAR)

        triangle_list = subdiv.getTriangleList()
        size = resized.shape
        r = (0, 0, size[1], size[0])

        for t in triangle_list:

            pt1 = (int(t[0] * fxy), int(t[1] * fxy))
            pt2 = (int(t[2] * fxy), int(t[3] * fxy))
            pt3 = (int(t[4] * fxy), int(t[5] * fxy))

            if utilities.rect_contains(r, pt1) and utilities.rect_contains(r, pt2) and utilities.rect_contains(r, pt3):
                cv2.line(resized, pt1, pt2, delaunay_color, 1)
                cv2.line(resized, pt2, pt3, delaunay_color, 1)
                cv2.line(resized, pt3, pt1, delaunay_color, 1)

        cv2.imshow(window_name, resized)
        if wait_key:
            cv2.waitKey()


def draw_voids(img, component, ratio, contours, error_ratio=25, contour_thickness=1, font_scale=2,
               font_thickness=3):
    """
    This function draws  the voids to the input image

    :param font_thickness:
    :param font_scale:
    :param contour_thickness:
    :param error_ratio: threshold value that will decide if the decect is fatal or not
    :param img: image where it will draw the voids and circles
    :param circle: input array containing [x,y,radius]
    :param ratio: ratio between void area and ball area
    :param contours: calculted contours
    :return: None
    """
    # visualize that the voids
    x_coordinate = int(component[0][0])
    y_coordinate = int(component[0][1])

    width = int(component[1][0])
    height = int(component[1][0])

    crop_raw = img[int(y_coordinate - height / 2):int(y_coordinate + height / 2),
               int(x_coordinate - width / 2):int(x_coordinate + width / 2)]

    if ratio > error_ratio:
        text = "Ratio: " + str(int(ratio)) + "%"
        cv2.drawContours(crop_raw, contours, -1, (0, 0, 255), thickness=contour_thickness)
        debug_show_rot_rects("Voids", img, [component], debug_mode=True, waitkey=False, color=(0, 0, 255))
        cv2.putText(img, text, (int(x_coordinate - width / 2), int(y_coordinate - height / 2)), cv2.FONT_HERSHEY_PLAIN,
                    fontScale=font_scale, color=(0, 0, 255), thickness=font_thickness)

    elif ratio == -1:
        text = "ERROR"
        cv2.drawContours(crop_raw, contours, -1, (0, 0, 255), thickness=contour_thickness)
        debug_show_rot_rects("Voids", img, [component], debug_mode=True, waitkey=False, color=(0, 0, 255))
        cv2.putText(img, text, (int(x_coordinate - width / 2), int(y_coordinate - height / 2)), cv2.FONT_HERSHEY_PLAIN,
                    fontScale=font_scale, color=(0, 0, 255), thickness=font_thickness)
    else:
        text = "Ratio: " + str(int(ratio)) + "%"
        cv2.drawContours(crop_raw, contours, -1, (0, 255, 0), thickness=contour_thickness)
        debug_show_rot_rects("Voids", img, [component], debug_mode=True, waitkey=False, color=(0, 255, 0))
        cv2.putText(img, text, (int(x_coordinate - width / 2), int(y_coordinate - height / 2)), cv2.FONT_HERSHEY_PLAIN,
                    fontScale=font_scale, color=(0, 255, 0), thickness=font_thickness)


def show_all_defects(base_img, test_img, debug_mode, fxy):
    """
    Visualuze the result of subtraction
    :param base_img: referential image
    :param test_img: tested image
    :param debug_mode: boolean
    :param fxy: resize factor
    :return:
    """
    # input images threshold
    base_blur = cv2.GaussianBlur(base_img, (5, 5), 0)
    thresh_value, base_bin = cv2.threshold(base_blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    test_blur = cv2.GaussianBlur(test_img, (5, 5), 0)
    test_bin = image_operations.use_thresholding(test_blur, method=1, custom_thresh=thresh_value)

    # debug print
    debug_show("base image bin", base_bin, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    debug_show("test image bin", test_bin, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # substract images
    negative_img = cv2.subtract(base_img, test_img)
    positive_img = cv2.subtract(test_img, base_img)
    substract_img = positive_img + negative_img
    debug_show("negative", negative_img, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    debug_show("positive", positive_img, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # threshold positive image
    positive_bin = image_operations.use_thresholding(positive_img, method=1, custom_thresh=25)
    kernel = np.ones((3, 3), np.uint8)
    positive_bin = cv2.morphologyEx(positive_bin, cv2.MORPH_OPEN, kernel, iterations=2)
    debug_show("Positive image binary", positive_bin, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # get contours of mussing components
    _, contours_positive, hierarchy_positive = cv2.findContours(positive_bin.copy(), cv2.RETR_TREE,
                                                                cv2.CHAIN_APPROX_NONE)

    # threshold negative image
    negative_bin = image_operations.use_thresholding(negative_img, method=1, custom_thresh=25)
    kernel = np.ones((3, 3), np.uint8)
    negative_bin = cv2.morphologyEx(negative_bin, cv2.MORPH_OPEN, kernel, iterations=2)
    debug_show("Negative image binary", negative_bin, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # get contours of mussing components
    _, contours_negative, hierarchy_negative = cv2.findContours(negative_bin.copy(), cv2.RETR_TREE,
                                                                cv2.CHAIN_APPROX_NONE)

    # output print
    base_color = cv2.cvtColor(base_img, cv2.COLOR_GRAY2BGR)
    show_multiple_contours("All defects - base image", base_color, [contours_positive, contours_negative],
                           [(0, 0, 255), (255, 0, 0)], debug_mode=True, fxy=fxy, waitkey=True, thickness=2)

    test_color = cv2.cvtColor(test_img, cv2.COLOR_GRAY2BGR)
    show_multiple_contours("All defects - test image", test_color, [contours_positive, contours_negative],
                           [(0, 0, 255), (255, 0, 0)], debug_mode=True, fxy=fxy, waitkey=True, thickness=2)


def show_missing_components(base_img, test_img, debug_mode, fxy):
    """
    Visualuze the result of subtraction -- focused on the missing component
    :param base_img: referential image
    :param test_img: tested image
    :param debug_mode: boolean
    :param fxy: resize factor
    :return:
    """
    # input images threshold
    base_blur = cv2.GaussianBlur(base_img, (5, 5), 0)
    thresh_value, base_bin = cv2.threshold(base_blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    test_blur = cv2.GaussianBlur(test_img, (5, 5), 0)
    test_bin = image_operations.use_thresholding(test_blur, method=1, custom_thresh=thresh_value)

    # debug print
    debug_show("base image bin", base_bin, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    debug_show("test image bin", test_bin, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # substract images
    negative_img = cv2.subtract(base_img, test_img)
    positive_img = cv2.subtract(test_img, base_img)
    substract_img = positive_img + negative_img
    debug_show("negative", negative_img, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    debug_show("positive", positive_img, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    debug_show("ADD", substract_img, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # threshold positive image
    positive_bin = image_operations.use_thresholding(positive_img, method=1, custom_thresh=25)
    kernel = np.ones((3, 3), np.uint8)
    positive_bin = cv2.morphologyEx(positive_bin, cv2.MORPH_OPEN, kernel, iterations=2)
    debug_show("Positive image binary", positive_bin, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # get contours of mussing components
    im2, contours, hierarchy = cv2.findContours(positive_bin.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    # output print
    base_color = cv2.cvtColor(base_img, cv2.COLOR_GRAY2BGR)
    show_resized_contours("Contours of the missing componets on base image", base_color.copy(), contours,
                          debug_mode=True, fxy=fxy, waitkey=True, thickness=1)
    test_color = cv2.cvtColor(test_img, cv2.COLOR_GRAY2BGR)
    show_resized_contours("Contours of the missing componets on tested image", test_color.copy(), contours,
                          debug_mode=True, fxy=fxy, waitkey=True, thickness=1)


def show_redundant_components(base_img, test_img, debug_mode, fxy):
    """
    Visualuze the result of subtraction -- focused on redundant components
    :param base_img: referential image
    :param test_img: tested image
    :param debug_mode: boolean
    :param fxy: resize factor
    :return:
    """
    # input images threshold
    base_blur = cv2.GaussianBlur(base_img, (5, 5), 0)
    thresh_value, base_bin = cv2.threshold(base_blur, 0, 255, cv2.THRESH_BINARY + cv2.THRESH_OTSU)
    test_blur = cv2.GaussianBlur(test_img, (5, 5), 0)
    test_bin = image_operations.use_thresholding(test_blur, method=1, custom_thresh=thresh_value)

    # debug print
    debug_show("base image bin", base_bin, debug_mode=debug_mode, fxy=fxy, waitkey=True)
    debug_show("test image bin", test_bin, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # substract images
    negative_img = cv2.subtract(base_img, test_img)
    debug_show("negative", negative_img, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # threshold positive image
    negative_bin = image_operations.use_thresholding(negative_img, method=1, custom_thresh=25)
    kernel = np.ones((3, 3), np.uint8)
    negative_bin = cv2.morphologyEx(negative_bin, cv2.MORPH_OPEN, kernel, iterations=2)
    debug_show("Negative image binary", negative_bin, debug_mode=debug_mode, fxy=fxy, waitkey=True)

    # get contours of mussing components
    im2, contours, hierarchy = cv2.findContours(negative_bin.copy(), cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)

    # output print
    base_color = cv2.cvtColor(base_img, cv2.COLOR_GRAY2BGR)
    show_resized_contours("Contours of the redundant componets on base image", base_color.copy(), contours,
                          debug_mode=True, fxy=fxy, waitkey=True, thickness=1)
    test_color = cv2.cvtColor(test_img, cv2.COLOR_GRAY2BGR)
    show_resized_contours("Contours of the redundant componets on tested image", test_color.copy(), contours,
                          debug_mode=True, fxy=fxy, waitkey=True, thickness=1)
