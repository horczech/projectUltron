\chapter{PCB defects}
Due to the high density and smaller size of the components on the PCB, the process of manufacturing of these boards is more prone to the defects than in the past. In general, we can divide these defects into two categories. The first category of defects are \textit{fatal defects} (sometimes called functional defects). This kind of defects can endanger the functionality of the PCB. This category includes for example short-circuit, open-circuit or missing hole. The second category are \textit{potential defects} (sometimes called cosmetic defects). This group of defects do not affect the functionality right after the manufacturing process, but they can compromise the PCB performance during utilization due to for example over heating.

This chapter describes the most common defects that can occur on the PCB and how they manifest on the x-ray image. The chapter \ref{bareDefects} will introduce defects that occur before the assembly and the chapter \ref{assemblyDefects} will describe defects that occur after the assembly process. Because the biggest potential of the AXI technology can be utilized after the PCB is assembled so the bare PCB defects will be introduced just briefly.

\section{Bare PCB defects} \label{bareDefects}
During the manufacturing process of the PCB (before the assembling) several defects can appear that are caused by an error in the process. This types of defects can be classified into defects caused by the missing copper and defects caused by the redundant copper. The source of this defects is usually dust, over etching, under etching or spurious metals.


The defects that are formed on the bare PCB are categorized into the categories described on in the table \ref{fig:barePCBdefectTable} and visualized in the Figure \ref{fig:barePCBdefectIMG}. \cite{barePCBdefect}

\begin{figure}[H]
    \centering
    \includegraphics[width=1\textwidth]{barePCBdefect}
    \caption{Classification of the bare PCB defects \cite{barePCBdefect}}
    \label{fig:barePCBdefectTable}
\end{figure}


\begin{figure}[H]
    \centering
    \includegraphics[width=1\textwidth]{barePCBdefetimg}
    \caption{Example of bare PCB defects \cite{barePCBdefect}}
    \label{fig:barePCBdefectIMG}
\end{figure}

These kinds of defects can be detected by using both AXI or AOI technology. In case of multilayer PCBs AOI is able to check only the outer sides of the board and in case of using the AXI technology it is possible to test all the layers separately by using the CT method. Depending on the complexity of the board it is also possible to use simple 2D x-ray image, but more complex boards will cause chaotic image caused by the overlapping of the conductors on each layer. 


There are two ways how to detect these kind of defects. User can compare the test image with some reference image and analyze the differences or it is also possible to check the geometry of the board. Detection methods are analyzed into more details in the chapter \ref{detectionMethods}.

\section{Assembly defects} \label{assemblyDefects}
Assembly defects is a group of defects that occur during the assembly of components to the PCB. The detection of these defects is crucial because it usually prevents the end product to meet its criteria. Thought understanding of the root causes of the defect manufacturers can improve the quality of  all assemblies. According to industry statistics, the top 3 PCB assembly defects which account for 74\% of all manufacturing defects are opens, solder bridging, and component shift. \cite{blog_AssemblyDefects}


\subsection{Open Solder Joints}
Open Solder Joint (sometimes called cold solder joint) occurs when there is no connection or poor connection between the lead and the pad which is causing an open connection. Some of these connection does not work at all, but some of them works but they are unreliable. The solder bond will be poor and the cracks may develop in the joint over time.

Detection of the is difficult using the top-down view so in order to detect open solder joint it is necessary to use a 3D CT or  to change the viewing angle from top-down to angle between 55 and 70 degrees. On the figure \ref{fig:openSolderJoint} you can see how does the open solder joint manifest itself on the oblique x-ray image. The red arrows are pointing toward open pins and the green arrows identify good pins.

This kind of defect accounts for 34\%  of all assembly defects and it can be caused by lack of solder paste, gap between the PCB and component or by corrosion at component lead. It can be prevented by trying to avoid paste contamination or ensuring proper heated solder paste. It can be repaired by  re-heating the joint with a hot iron until the solder flows or adding more solder. 



\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{openSolderJoint}
    \caption{Open solder joints of the QFN package \cite{openSolderJointQFN}}
    \label{fig:openSolderJoint}
\end{figure}



\subsection{Solder bridges (shorts)}
Solder bridges can occur in case of melting  of two solder joints together which cause unintended connection. Shorts can be microscopic so they can be very difficult to detect visually and it can potentially cause serious damage to the components such as burn-out or it can damage a PCB by burning-out of the trace and causing open circuit. For detection of this kind of defect it is possible to use 2D top-down view. For example, on the figure \ref{fig:shortBGA} you can clearly see that two solder balls in the BGA melted together underneath the component package. 



This defect can be caused by applying too much solder on the pads, misalignment  between the stencil and PCB or because of the soldering pads are  too big in compassion to the gab. In some cases this defect can be repaired manually by scraping out unwanted connection or dragging the tip of a hot iron between the two solder joints. If there is too much solder, a solder sucker or solder wick can help get rid of the excess.


\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{shortBGA}
    \caption{Solder bridge of two solder balls \cite{shortBGA}}
    \label{fig:shortBGA}
\end{figure}

\subsection{Component Shift}
Component shift manifests itself as a misalignment  between the component and the solder pad. It usually happens during the reflow soldering due to the components ability to float on the molten solder. It can be caused because of  the oxidation of component leads, bent leads, vibrations and in case of placing small component next to the large component where the heated gas is directed from the side of big component towards the  smaller ones. Component shift can be prevented by minimizing of the movement of the unreflowed assembly boards, using more aggressive flux and compliance the requirement temperature and humidity.

On the figure \ref{fig:shiftedComponent} you can see example of shifted component. This kind of defect can be detected by comparison of top-down view of the reference image of the PCB with the tested image. 


\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{shiftedComponent}
    \caption{Shifted component}
    \label{fig:shiftedComponent}
\end{figure}

\subsection{Tombstone}
Tombstone is an extreme version of component shift. It occurs when  one side of the \zkratka{SMT} component is properly soldered to the the PCB pad and the other one stands up vertically. There are several reasons why it can occur, one of them is different wedding speeds that can cause imbalanced torque  on each side of the component. The uneven oven temperature, nitrogen presence or the uneven solder paste printing can  increase the occurrence of the tombstone defect.

On the figure \ref{fig:tombleStoneDefect} you can see example of the tombstone defect. This kind of defect can be identified using the top-down view of the board because it also usually cause shift of the component to the side, but the better option is to use the CT or oblique x-ray image.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{tombleStoneDefect}
    \caption{Tombstone \cite{Tombstone}}
    \label{fig:tombleStoneDefect}
\end{figure}

\subsection{BGA Voids}
BGA voids or voids in general occur when using the surface mount technology during the rewlow process. Enclosed voids can cause displacement of electrical or heat paths that can lead to local overheating. Gas voids usually form balls inside the solder ball which could lead to tilting of the component. The void occurrence can be affected by the e.g. a good wettability of metallization, solder pastes with special adopted solvents or an adequate preheating profile.

As you can see on the figure  \ref{fig:bgaVoidsDef} BGA voids manifest itself on the x-ray image as a bright spots inside the solder ball. This kind of defect can be detected by segmenting each solder ball and calculating the ratio the surface between the solder ball and the void area. This can be done by using the top-down image of the PCB, but that will give us just the area of the void so for the more precise calculation of the volume of the void it is necessary to use the CT scan of the board. For more information about the BGA void detection read the chapter \ref{BGAvoidDetection} where is the problem described into more detail.


\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\textwidth]{bgaVoidsDef}
    \caption{Voids inside the solder balls}
    \label{fig:bgaVoidsDef}
\end{figure}

