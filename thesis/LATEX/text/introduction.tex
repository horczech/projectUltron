\chapter{Introduction}
Detection of defects during the manufacturing process of the \zkratka{PCBs} is a never-ending process that is driven by the desire to make the assembly process more efficient.
Because of the great increase of demand of the electronic devices, manufacturers of PCBs have taken very important place. This great demand also introduced a lot of new challenges. They are for example expected to deliver more circuit boards, cheaper with higher quality. Because the lucrative market with the \zkratka{PCBs} reached an estimated \$60.2 billion in value in 2014, a lot of effort is put to improve the process of manufacturing.\footnote{http://www.ipc.org/ContentPage.aspx?pageid=World-PCB-Production-in-2014-Estimated-at-60-2-Billion.} This thesis  focuses on the inspection part of the manufacturing process especially on the visual inspection using the x-ray and machine vision because by using these technologies manufacturers can achieve high reliability and reduce repair costs. The goal of the inspection is not to only find fatal defects such as breakouts, bridges or missing conductors that will compromise the \zkratka{PCB} performance during utilization, but also to find potential defects that can cause troubles in the longer time period such as over etching, under etching or voids in the \zkratka{BGA}.


With the constant improvement of technologies manufacturers are pushed towards smaller, lighter and more functional devices where every millimeter on the board has to be fully utilized so the circuit boards have evolved into very complex, multilayer, high density boards. Testing of such a boards have become more challenging task because higher component and joint counts create more defect opportunities which lead to lower yields for a given defect level.
Constant increase in quality demands and complexity of circuit boards are forcing manufacturers to develop new methods of inspection that will ensure inline control of every circuit board and separate those that does not fulfill the quality requirements. By using the inline inspection, manufacturer can use collected data not only for identifying the bad pieces but also as a feedback loop to improve the manufacturing process.


\section{Inspection methods}
In the early days of PCB manufacturing, all inspection was undertaken manually by humans with the magnification glass. Later, this method showed up to be very limited not only because the human factor, but also because of its speed and limited ability to spot defects that are invisible for human eye. Because of the rapid growth of the PCB manufacturing industry manufacturers invested into developing new methods that would keep up with the growing requirements of the customers.  That resulted in a wide range of test and inspection strategies that will be briefly introduced in this chapter.

\begin{enumerate}
\item \textbf{Visual Inspection} \\
Visual method is the traditional way of \zkratka{PCB} inspection, but it is still widely used. It utilizes the magnifying glass or microscope and it is controlled by trained human controllers. The advantage of this approach is that there are low initial costs, but because of increasing complexity of circuit boards and miniaturization it is becoming more challenging. The great disadvantage of this approach is that the quality of control is depended on each individual, it is not effective in mass production and it is also more difficult to collect the data over time for analysis of the production. On top of that some defects can not be judged only by visual inspection system so it does not ensure 100\% reliability.  

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{visual_inspection}
    \caption{Example of visual inspection \cite{visualInspection}}
    \label{fig:visualInspection}
\end{figure}



\item \textbf{Solder Paste Inspection } \\
Solder paste inspection (SPI) is a method that tests the solder paste deposit  by measuring the volume of solder pads before the components are applied and the solder melted. This inspection method is necessary especially when the lead free solder pastes are used because it has been proven that they do not spread as well as tin lead solder pastes. Based on the image capturing method SPI can be divided into a 2D (area coverage) or 3D (volume coverage) inspection. \cite{SPI-metody}



\item \textbf{Functional Test (FT) } \\
Functional test is the most straight forward way how to test a PCB board. It is little bit different from other forms of testing because it is not testing whether the solder is there or not. It receives an assembled product and it simulates the electrical environment where should the circuit board work. For instance, if the final PCB is going to be a slot inside a computer than the slot will be simulated during the tests. It can be considered as a final quality control that will make sure that the end product meets the requirements.  The drawback of this approach is that it is necessary to mimic the environment for every type of the PCB, it also might be very difficult to sufficiently test a systems with moderate complexity. A subcategory of the functional tests is \textit{structural test} which does not test the PCB as a whole, but it tests individual partitions. This method allows to test the functionality more deeply  because it can verify the corner cases, but it is difficult to reach necessary pins in the more complex or multiple layer PCBs.


\item \textbf{Boundary scan} \\
Boundery scan (also known as JTAG boundary-scan) is a method for testing of  interconnects on the \zkratka{PCB} or it can be also used as debugging method to watch integrated circuit pin states. It is a way how to overcome challenges connected with mechanical access to the pins of the component by adding dedicated circuit tree to the system ICs. It uses special registers that allow to observe the signals or to input custom input and test its functionality. A drawback of this solution is that when the manufacturer wants to utilize this kind of test it is necessary include it to the design of the PCB.




\item \textbf{In-circuit test } \\
In-circuit test (ICT) method is based on testing electrical parameters and performance of the circuit board to identify shorts, opens, resistance, capacitance, and other basic quantities which will show whether the assembly was correctly manufactured. There are two commonly used methods. The first one is the \textit{Flying Probe method} which is basically several probes that are moved above the measured board by two axis system and test the circuit (see Figure \ref{fig:flyingPropeTest}). The second method is called \textit{bed of nails test} which has numerous pins that are adjusted to the measured circuit board (see figure \ref{fig:needleBedTest}). This method more suitable to high volume production because it is much faster than \textit{flying probe test}, but on the other hand this method needs a fixture for each kind of \zkratka{PCBs} and the cost is high. This method was very effective in the past, but with the increasing density of components this method is becoming restricted. 

Nowadays, these methods are not very effective due loss of physical access to the components and space constraints.  


\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{flying_probe_test}
    \caption{Flying probe test \cite{ICTtesting}}
    \label{fig:flyingPropeTest}
\end{figure}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{needle_bed_test}
    \caption{Needle bed test \cite{ICTtesting}}
    \label{fig:needleBedTest}
\end{figure} 


\item \textbf{Automatic optical inspection} \\
Automatic optical inspection (AOI) is the advanced method that can be used to identify defects using the camera. It is non-contact method therefore it is very flexible because it does not need any fixture.  Automatic optical inspection can work with both bare or assembled circuits and it can be used to find defects of the circuit board such as open-circuits, short-circuit defects, missing components, offset or incorrectly mounted parts. On top of that this method is fast enough to run online and do the proper testing of each circular board.
The \zkratka{AOI} consists of three steps. In the first step it obtains the image of the tested board. This image can be captured by \zkratka{CCD} camera. The crucial part of capturing of the image is lighting because \zkratka{PCB} has high reflection which will cause shadows of the object so in order to get good results it is essential to have very good light source. The captured image is processed and useful features are extracted. Finally the result can be processed by a computer program that will find defects automatically by using testing the geometrical rules of the PCB or by comparing with the referential circuit board. The disadvantage of this method is that it can test only the surface of the \zkratka{PCB} so it can not reveal defects that does not manifest itself on the surface of the board.


\item \textbf{Automatic X-ray inspection} \\
Automatic X-ray inspection (AXI) is very similar to the \zkratka{AOI}. The only difference is that the way how is the image captured. This method is measuring how much light got absorbed by the \zkratka{PCB} at given point. The great advantage is that it is giving us more information about the PCB than the  \zkratka{AOI}. We can find for example bubbles (usually referred as voids) in the solder balls  which is impossible to reveal using the CCD camera. On the other hand in case of multiple layer boards it might be more challenging to process the captured image due to the interference which is caused by components on the top and bottom layer of the PCB that are overlapping.
The basic x-ray  machine setup will provide a 2D image of the board, but more  advanced machines can capture 2.5D image or even CT scan of the board (see Figure \ref{fig:BGA_3D}). 

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{BGA_3D}
    \caption{CT scan of the BGA \cite{CtBGAImage}}
    \label{fig:BGA_3D}
\end{figure} 

\end{enumerate}

\section{The best inspection method}
There is no such thing as "the best inspection method" or method that can detect all possible defects. 
In fact choosing the right inspection method is very complex question and the answer is usually combination of multiple methods. Even picking between AOI and AXI can be tough question because is not just simple matter of looking at few characteristics. There are a lot of factors that have to be considered such as volume of the production, type of components, physical accessibility to the components, potential sources of defects and many others. 

A lot of studies were done in order to provide some data that will help manufacturers to decide which method to pick. For example Stig Oresjo from Agilent Technologies Loveland in Colorado published study that was comparing efficiency of AOI, AXI and ICT methods after the steps of the PCB manufacturing process mentioned in the Table \ref{tab:ManufactureSteps}

% Please add the following required packages to your document preamble:
% \usepackage[table,xcdraw]{xcolor}
% If you use beamer only pass "xcolor=table" option, i.e. \documentclass[xcolor=table]{beamer}
\begin{table}[h]
\centering
\caption{When each inspection method was used}
\label{tab:ManufactureSteps}
\begin{tabular}{|l|l|l|l|}
\hline
\rowcolor[HTML]{EFEFEF} 
{\color[HTML]{333333} \textbf{Process step}} & {\color[HTML]{333333} \textbf{AOI}} & {\color[HTML]{333333} \textbf{AXI}} & {\color[HTML]{333333} \textbf{ICT}} \\ \hline
Post pick-and-place, Pre-flow                & X                                   &                                     &                                     \\ \hline
Post-reflow, Pre-wave                        & X                                   & X                                   &                                     \\ \hline
Post-wave                                    &                                     & X                                   & X                                   \\ \hline
\end{tabular}
\end{table}

\begin{figure}[h]
    \centering
    \includegraphics[width=0.5\textwidth]{inspection-methods}
    \caption{Number of defects found by each method \cite{Inspection-methods-comparison}}
    \label{fig:inspection}
\end{figure} 

The Venn diagram on the Figure \ref{fig:inspection} shows how many defects were detected by each inspection method. You can see that the most effective method is AXI which detected 130  out of 142 defects. The Venn diagram also clearly shows that the AXI method is able to detect most of the defects detected by AOI, but not all of them. On the other hand there is a ICT method itself detected just 22\% of the defects.  \cite{Inspection-methods-comparison}




\section{Computer vision}
Computer vision started in the late 1960s but the massive expansion started recently thanks to the increasing performance and decreasing price of the computers. Today, machine vision became a part of  a lot of fields starting with smartphones, through automotive industry up to PCB manufacturing industry trying to provide additional feedback about the surrounding environment.

X-ray inspection or optical inspection using the CCD camera will not be so beneficial without computer vision which goal is to mimic behavior of the human inspector. That means that the system should not only capture the environment in front of the camera, but also understand what is happening and make decisions based on the information extracted form the image. These systems have a lot of advantages over the human such as speed, no subjectivity or mistakes caused by fatigue and ability to work without any break. Even though the initial costs of the systems using the computer vision is very high it is beneficial in the long term especially in the high volume production.


\section{What is defect?}
The word \textit{defect} is frequently used in this thesis so it is important to properly explain what is meant by this word. According to the Oxford Dictionary "defect" is defined as \textit{"A shortcoming, imperfection, or lack}". Which is very accurate definition even in the context of the printed circuit boards. In general, \textit{defect} can be descried as a deviation from the norm that results in modification of the production process and prevent the defects that can affect the proper functionality or reliability of the PCB to prevent the costs connected with complaints. This implies that defect does not have to be necessarily something that prevent the PCB to work properly, but defect can be also an imperfection whose solving can make the overall manufacturing process more efficient and reliable.







