import cv2
from Ultron.enums import CircleDetectionMethod
from Ultron.utilities import output
from Ultron.enums import EdgeDetectioMethod
from Ultron.utilities import find_functions
from Ultron import edge_detection


image_path = '.../images/figure.jpg'
image = cv2.imread(image_path)
solderballs = find_functions.find_solderballs(image, method=CircleDetectionMethod.BoundaryRectangles,
                                              debug_mode=True, fxy=1)
method = edge_detection.MethodLoG(gaus_blur_kernel=5, laplace_kernel=15)
solderballs.analyze_voids(image, method=method, debug_mode=True)
solderballs.show_all_voids(image, error_ratio=25)
