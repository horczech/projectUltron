\select@language {english}
\contentsline {chapter}{\numberline {1}Introduction}{11}{chapter.5}
\contentsline {section}{\numberline {1.1}Inspection methods}{11}{section.7}
\contentsline {section}{\numberline {1.2}The best inspection method}{15}{section.19}
\contentsline {section}{\numberline {1.3}Computer vision}{17}{section.22}
\contentsline {section}{\numberline {1.4}What is defect?}{17}{section.23}
\contentsline {chapter}{\numberline {2}Automated X-ray inspection}{18}{chapter.24}
\contentsline {section}{\numberline {2.1}Basic properties of x-ray images of PCB}{18}{section.25}
\contentsline {section}{\numberline {2.2}3D X-ray Computer Tomography }{19}{section.28}
\contentsline {section}{\numberline {2.3}AXI technology features}{20}{section.30}
\contentsline {chapter}{\numberline {3}PCB defects}{22}{chapter.32}
\contentsline {section}{\numberline {3.1}Bare PCB defects}{22}{section.33}
\contentsline {section}{\numberline {3.2}Assembly defects}{24}{section.36}
\contentsline {subsection}{\numberline {3.2.1}Open Solder Joints}{24}{subsection.37}
\contentsline {subsection}{\numberline {3.2.2}Solder bridges (shorts)}{25}{subsection.39}
\contentsline {subsection}{\numberline {3.2.3}Component Shift}{26}{subsection.41}
\contentsline {subsection}{\numberline {3.2.4}Tombstone}{26}{subsection.43}
\contentsline {subsection}{\numberline {3.2.5}BGA Voids}{27}{subsection.45}
\contentsline {chapter}{\numberline {4}Programming languages for computer vision}{29}{chapter.47}
\contentsline {section}{\numberline {4.1}Matlab}{29}{section.48}
\contentsline {section}{\numberline {4.2}C++}{30}{section.49}
\contentsline {section}{\numberline {4.3}C sharp}{30}{section.50}
\contentsline {section}{\numberline {4.4}Python}{31}{section.51}
\contentsline {section}{\numberline {4.5}Why Python?}{31}{section.52}
\contentsline {chapter}{\numberline {5}Defect detection methods}{33}{chapter.53}
\contentsline {section}{\numberline {5.1}Reference comparison}{33}{section.55}
\contentsline {section}{\numberline {5.2}Non-reference comparison}{34}{section.62}
\contentsline {section}{\numberline {5.3}Hybrid inspection}{35}{section.63}
\contentsline {section}{\numberline {5.4}Machine Learnig/ Neural networks/Data mining}{35}{section.64}
\contentsline {chapter}{\numberline {6}Implementation}{36}{chapter.66}
\contentsline {section}{\numberline {6.1}BGA void detection}{36}{section.67}
\contentsline {subsection}{\numberline {6.1.1}Solder ball segmentation}{36}{subsection.69}
\contentsline {subsection}{\numberline {6.1.2}Void area calculation}{42}{subsection.86}
\contentsline {subsection}{\numberline {6.1.3}Program usage}{45}{subsection.97}
\contentsline {subsection}{\numberline {6.1.4}Conclusion}{48}{subsection.117}
\contentsline {section}{\numberline {6.2}Thermal pad void detection in the QFN package}{49}{section.119}
\contentsline {subsection}{\numberline {6.2.1}QFN package segmentation}{49}{subsection.121}
\contentsline {subsection}{\numberline {6.2.2}QFN void detection}{52}{subsection.136}
\contentsline {subsection}{\numberline {6.2.3}Program usage}{54}{subsection.140}
\contentsline {section}{\numberline {6.3}Image subtraction}{55}{section.161}
\contentsline {subsection}{\numberline {6.3.1}Image alignment}{57}{subsection.166}
\contentsline {subsection}{\numberline {6.3.2}Defect classification}{60}{subsection.184}
\contentsline {subsection}{\numberline {6.3.3}Program usage}{61}{subsection.186}
\contentsline {chapter}{\numberline {7}Conclusion}{63}{chapter.215}
\contentsline {chapter}{Bibliography}{65}{section*.217}
\contentsline {chapter}{List of symbols, physical constants and abbreviations}{70}{section*.219}
\contentsline {chapter}{List of appendices}{71}{section*.221}
