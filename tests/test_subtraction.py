"""

    Author: Martin Horak
    Python Version: 3.5
    OpenCV Version: 3.1.0
    Description: Testing of the functions that are connected with the image subtraction

"""


import cv2
from Ultron.utilities import image_operations
import os
import pytest



def test_should_raiseValueError_when_noPcbOnImage():
    image_path = os.path.join(os.getcwd(), 'test_images', 'empty_image.jpg')
    image = cv2.imread(image_path)
    img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    with pytest.raises(ValueError):
        image_cropped = image_operations.crop_pcb(img_gray, debug_mode=False, fxy=1, pcb_thresholding=180)

def test_should_return45Degrees_when_pcbIs45DegreesRotated():
    image_path = os.path.join(os.getcwd(), 'test_images', 'rotation_test_image_45.jpeg')
    image = cv2.imread(image_path)
    img_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    pcb_boundary = ((1401.741943359375, 1376.6036376953125), (1670.7518310546875, 1092.030517578125), -54.9316520690918)
    rotated_img, (x_min, x_max, y_min, y_max) = image_operations.make_pcb_upright(img_gray, pcb_boundary)

    assert x_min == 1108 and x_max == 2779 and y_min == 1480 and y_max == 2572


