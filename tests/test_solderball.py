"""

    Author: Martin Horak
    Python Version: 3.5
    OpenCV Version: 3.1.0
    Description: Testing of the functions that are connected with the solder ball segmentation and void detection

"""

import Ultron.utilities.find_functions
import cv2
from Ultron.utilities import output
from Ultron.utilities import find_functions
from Ultron.enums import EdgeDetectioMethod
import os
from Ultron.enums import CircleDetectionMethod
import pytest


def test_should_find63solderballs_when_methodIsBoundaryRectangles():
    image_path = os.path.join(os.getcwd(), 'test_images', 'fig2.jpg')
    image = cv2.imread(image_path)
    solderballs = find_functions.find_solderballs(image, fxy=1, method=CircleDetectionMethod.BoundaryRectangles)

    assert len(solderballs.angle) == 63


def test_should_find63solderballs_when_methodIsHoughtCircles():
    image_path = os.path.join(os.getcwd(), 'test_images', 'fig2.jpg')
    image = cv2.imread(image_path)
    solderballs = find_functions.find_solderballs(image, fxy=1, method=CircleDetectionMethod.HoughtCircles)

    assert len(solderballs.angle) == 63

def test_should_raiseExceptionAttributeError_when_methodIsWrong():
    image_path = os.path.join(os.getcwd(), 'test_images', 'fig2.jpg')
    image = cv2.imread(image_path)

    with pytest.raises(AttributeError):
        solderballs = find_functions.find_solderballs(image, fxy=1, method=11234)


def test_should_raiseExceptionSystemExit_when_methodNoSolderballs():
    image_path = os.path.join(os.getcwd(), 'test_images', 'empty_image.jpg')
    image = cv2.imread(image_path)

    with pytest.raises(SystemExit):
        solderballs = find_functions.find_solderballs(image, fxy=1, method=CircleDetectionMethod.BoundaryRectangles)


def test_should_raiseExceptionAttributeError_when_voidMethodIsWrong():
    image_path = os.path.join(os.getcwd(), 'test_images', 'fig2.jpg')
    image = cv2.imread(image_path)

    with pytest.raises(AttributeError):
        solderballs = find_functions.find_solderballs(image, fxy=1, method=11234)

def test_should_raiseExceptionAttributeError_when_userEntersWrongVoidDetectionMethod():
    image_path = os.path.join(os.getcwd(), 'test_images', 'fig2.jpg')
    image = cv2.imread(image_path)
    solderballs = find_functions.find_solderballs(image, debug_mode=False, fxy=1,
                                                  method=CircleDetectionMethod.BoundaryRectangles)

    with pytest.raises(AttributeError):
        solderballs.analyze_voids(image, method=EdgeDetectioMethod.LoGaad, debug_mode=False)
