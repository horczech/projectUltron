"""

    Author: Martin Horak
    Python Version: 3.5
    OpenCV Version: 3.1.0
    Description: Testing of the functions that are connected with the qnf package segmentation

"""

import cv2
from Ultron.utilities import find_functions
import os
import pytest


def test_should_raiseExceptionSystemExit_when_methodNoSolderballs():
    image_path = os.path.join(os.getcwd(), 'test_images', 'empty_image.jpg')
    image = cv2.imread(image_path)

    with pytest.raises(SystemExit):
        qnf_packages = find_functions.find_qnf_package(image, is_square_package=False, is_bevelled_edge=True,
                                                       k_size=11,
                                                       iterrations=11, debug_mode=False, fxy=1)

def test_should_return1_when_fig8Entered():
    image_path = os.path.join(os.getcwd(), 'test_images', 'fig8.png')
    image = cv2.imread(image_path)

    qnf_packages = find_functions.find_qnf_package(image, is_square_package=False, is_bevelled_edge=True,
                                                   k_size=11,
                                                   iterrations=11, debug_mode=False, fxy=1)
    assert len(qnf_packages.angle) == 1