try:
    from setuptools import setup
except ImportError:
    from distutils.core import setup

setup(
    name='Ultron',
    version='1.0',
    description='Automatic defect detection on x-ray image of PCB',
    author='Martin Horak',
    author_email='horczech@cgmail.com',
    url='https://gitlab.com/horczech/projectUltron/tree/master',
    packages=['Ultron', 'Ultron.component', 'Ultron.defect', 'Ultron.utilities'],
)

